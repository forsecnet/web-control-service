/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.must;


import com.forsecnet.polri.dashboardback.model.bhabin.BhabinComment;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author user
 */
@Entity
@Table(name = "reports", schema = "must")
@SQLDelete(sql = "UPDATE must.reports SET report_status = 'DITOLAK', report_deleted_at = now() WHERE report_id = ? AND report_updated_at = ?")
public class MustReport implements Serializable {
    


    public enum Category { 
        KEJAHATAN,
        GANGGUAN_KETERTIBAN_WARGA,
        MASALAH_LALU_LINTAS,
        TEROR_BOM,
        GELANDANGAN_ORANG_HILANG,
        NARKOBA,
        PERSELISIHAN_WARGA,
        BENCANA,
        POTENSI_KONFLIK_SARA
    }

    public enum Status {
        MENUNGGU,
        PROSES,
        TUNTAS,
        DITOLAK
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private long id;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "picture_file")
    @CollectionTable(name="report_reported_pictures", schema = "must", joinColumns = @JoinColumn(name="report_id"))
    @OrderColumn(name = "picture_index")
    private List<String> reportedPictureFiles;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "picture_file")
    @CollectionTable(name="report_solved_pictures", schema = "must", joinColumns = @JoinColumn(name="report_id"))
    @OrderColumn(name = "picture_index")
    private List<String> solvedPictureFiles;
    
    @Column(name = "report_title")
    private String title;
    
    @Column(name = "report_description")
    private String description;
    
    @OneToOne
    @JoinColumn(name = "position_id")
    private MustPosition mustPosition;
    
    @ManyToOne
    @JoinColumn(name = "handler_id")
    private BhabinProfile handler;
    
    @OneToMany(mappedBy = "mustReport")
    private List<BhabinComment> bhabinComments;
    
    @OneToMany(mappedBy = "mustReport")
    private List<MustComment> mustComments;
    
    @Column(name = "report_rating")
    private int rating;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "report_category")
    private Category category;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "report_status")
    private Status status;
    
    @Column(name = "report_responded_at", nullable = true)
    private Timestamp respondedAt;
    
    @Column(name = "report_completed_at", nullable = true)
    private Timestamp completedAt;
    
    @Column(name = "report_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;
    
    @Version
    @Column(name = "report_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "report_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getReportedPictureFiles() {
        return reportedPictureFiles;
    }

    public void setReportedPictureFiles(List<String> reportedPictureFiles) {
        this.reportedPictureFiles = reportedPictureFiles;
    }

    public List<String> getSolvedPictureFiles() {
        return solvedPictureFiles;
    }

    public void setSolvedPictureFiles(List<String> solvedPictureFiles) {
        this.solvedPictureFiles = solvedPictureFiles;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MustPosition getMustPosition() {
        return mustPosition;
    }

    public void setMustPosition(MustPosition mustPosition) {
        this.mustPosition = mustPosition;
    }

    public BhabinProfile getHandler() {
        return handler;
    }

    public void setHandler(BhabinProfile handler) {
        this.handler = handler;
    }

    public List<BhabinComment> getBhabinComments() {
        return bhabinComments;
    }

    public void setBhabinComments(List<BhabinComment> bhabinComments) {
        this.bhabinComments = bhabinComments;
    }

    public List<MustComment> getMustComments() {
        return mustComments;
    }

    public void setMustComments(List<MustComment> mustComments) {
        this.mustComments = mustComments;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Timestamp getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Timestamp respondedAt) {
        this.respondedAt = respondedAt;
    }

    public Timestamp getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Timestamp completedAt) {
        this.completedAt = completedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

}
