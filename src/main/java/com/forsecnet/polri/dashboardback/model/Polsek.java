package com.forsecnet.polri.dashboardback.model;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;


@Entity
@Table(name = "polsek", schema = "common")
@SQLDelete(sql = "UPDATE common.polsek SET polsek_deleted_at = now() WHERE polsek_id = ? AND polsek_updated_at = ?")
public class Polsek implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "polsek_id")
    private long id;
    
    @Column(name = "polsek_name")
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "polres_id")
    private Polres polres;
    
    @OneToMany(mappedBy = "polsek")
    private List<BhabinProfile> bhabinProfiles;
    
    @Column(name = "polsek_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "polsek_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "polsek_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Polres getPolres() {
        return polres;
    }

    public void setPolres(Polres polres) {
        this.polres = polres;
    }

    public List<BhabinProfile> getBhabinProfiles() {
        return bhabinProfiles;
    }

    public void setBhabinProfiles(List<BhabinProfile> bhabinProfiles) {
        this.bhabinProfiles = bhabinProfiles;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
    
    
}
