/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "credentials", schema = "bhabin", indexes = {
        @Index(name = "bhabin_credential_nrp_unique", unique = true, columnList = "credential_nrp") })
@SQLDelete(sql = "UPDATE bhabin.credentials SET credential_deleted_at = now() WHERE credential_id = ? AND credential_updated_at = ?")
public class BhabinCredential implements Serializable, UserDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "credential_id")
    private long id;

    @OneToOne(mappedBy = "bhabinCredential")
    private BhabinProfile bhabinProfile;

    @Column(name = "credential_password")
    private String password;

    @Column(name = "credential_nrp")
    private String nrp;

    @Column(name = "credential_validated_at", nullable = true)
    private Timestamp validatedAt;

    @Column(name = "credential_banned_at", nullable = true)
    private Timestamp bannedAt;

    @Column(name = "credential_created_at", nullable = false, insertable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "credential_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "credential_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BhabinProfile getBhabinProfile() {
        return bhabinProfile;
    }

    public void setBhabinProfile(BhabinProfile bhabinProfile) {
        this.bhabinProfile = bhabinProfile;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public Timestamp getValidatedAt() {
        return validatedAt;
    }

    public void setValidatedAt(Timestamp validatedAt) {
        this.validatedAt = validatedAt;
    }

    public Timestamp getBannedAt() {
        return bannedAt;
    }

    public void setBannedAt(Timestamp bannedAt) {
        this.bannedAt = bannedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ROLE_BHABIN");
    }

    @Override
    @Transient
    public String getUsername() {
        return nrp;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return deletedAt == null;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return bannedAt == null;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return deletedAt == null;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return validatedAt != null;
    }
}
