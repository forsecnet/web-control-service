/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import com.forsecnet.polri.dashboardback.model.MustReportCommenter;
import com.forsecnet.polri.dashboardback.model.Polsek;
import com.forsecnet.polri.dashboardback.model.TierThreeRegion;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "profiles", schema = "bhabin")
@SQLDelete(sql = "UPDATE bhabin.profiles SET profile_deleted_at = now() WHERE credential_id = ? AND profile_updated_at = ?")
public class BhabinProfile implements Serializable, MustReportCommenter {


    private static final String COMMENTER_ROLE = "BHABIN";

    public enum Rank {
        AIPTU, AIPDA, BRIGADIR, BRIPKA, BRIPTU
    }

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "bhabinCredential"))
    @Column(name = "credential_id")
    private long id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "credential_id")
    private BhabinCredential bhabinCredential;

    @Column(name = "profile_name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "profile_rank")
    private Rank rank;
    
    @ManyToOne
    @JoinColumn(name = "polsek_id")
    private Polsek polsek;

    @Column(name = "profile_phone")
    private String phone;
    
    @Column(name = "profile_home_address")
    private String homeAddress;

    @ManyToMany
    @JoinTable(name = "profile_region_assignments", schema = "bhabin", joinColumns = @JoinColumn(name = "credential_id", referencedColumnName = "credential_id"), inverseJoinColumns = @JoinColumn(name = "tier_three_region_id", referencedColumnName = "tier_three_region_id"))
    private List<TierThreeRegion> tierThreeRegions;

    @Column(name = "profile_picture_file")
    private String pictureFile;

    @OneToMany(mappedBy = "bhabinProfile")
    @OrderBy("createdAt ASC")
    private List<BhabinPosition> positions;

    @Column(name = "profile_time_offset")
    private long timeOffset;

    @Column(name = "profile_last_seen_at")
    private Timestamp lastSeenAt;

    @Column(name = "profile_created_at", nullable = false, insertable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "profile_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "profile_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BhabinCredential getBhabinCredential() {
        return bhabinCredential;
    }

    public void setBhabinCredential(BhabinCredential bhabinCredential) {
        this.bhabinCredential = bhabinCredential;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Polsek getPolsek() {
        return polsek;
    }

    public void setPolsek(Polsek polsek) {
        this.polsek = polsek;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public List<TierThreeRegion> getTierThreeRegions() {
        return tierThreeRegions;
    }

    public void setTierThreeRegions(List<TierThreeRegion> tierThreeRegions) {
        this.tierThreeRegions = tierThreeRegions;
    }

    @Override
    public String getPictureFile() {
        return pictureFile;
    }

    public void setPictureFile(String pictureFile) {
        this.pictureFile = pictureFile;
    }

    public List<BhabinPosition> getPositions() {
        return positions;
    }

    public void setPositions(List<BhabinPosition> positions) {
        this.positions = positions;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(long timeOffset) {
        this.timeOffset = timeOffset;
    }

    public Timestamp getLastSeenAt() {
        return lastSeenAt;
    }

    public void setLastSeenAt(Timestamp lastSeenAt) {
        this.lastSeenAt = lastSeenAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    @Transient
    public String getCommenterRole() {
        return COMMENTER_ROLE;
    }

}
