/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model;

/**
 *
 * @author ahmad
 */
public interface MustReportCommenter {
    public String getName();
    public String getPictureFile();
    public String getCommenterRole();
}
