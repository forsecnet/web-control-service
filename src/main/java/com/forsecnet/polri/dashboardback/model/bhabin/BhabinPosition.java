/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "positions", schema = "bhabin")
public class BhabinPosition implements Serializable {
    


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "position_id", updatable = false)
    private long id;
     
    @ManyToOne
    @JoinColumn(name = "credential_id", updatable = false)
    private BhabinProfile bhabinProfile;
    
    @Column(name = "position_latitude", updatable = false)
    private double latitude;
    
    @Column(name = "position_longitude", updatable = false)
    private double longitude;
    
    @Column(name = "position_accuracy", updatable = false)
    private double accuracy;
    
    @Column(name = "position_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BhabinProfile getBhabinProfile() {
        return bhabinProfile;
    }

    public void setBhabinProfile(BhabinProfile bhabinProfile) {
        this.bhabinProfile = bhabinProfile;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
    
}
