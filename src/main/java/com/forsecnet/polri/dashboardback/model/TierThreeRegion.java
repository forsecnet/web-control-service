/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "tier_three_regions", schema = "common")
@SQLDelete(sql = "UPDATE common.tier_three_regions SET tier_three_region_deleted_at = now() WHERE tier_three_region_id = ? AND tier_three_region_updated_at = ?")
public class TierThreeRegion implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tier_three_region_id")
    private long id;
    
    @Column(name = "tier_three_region_name")
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "tier_two_region_id")
    private TierTwoRegion tierTwoRegion;
    
    @Column(name = "tier_three_region_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "tier_three_region_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "tier_three_region_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public TierTwoRegion getKecamatan() {
        return tierTwoRegion;
    }

    public void setKecamatan(TierTwoRegion tierTwoRegion) {
        this.tierTwoRegion = tierTwoRegion;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
}
