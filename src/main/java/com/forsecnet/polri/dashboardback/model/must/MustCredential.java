/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.must;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author user
 */
@Entity
@Table(name = "credentials", schema = "must", indexes = {
        @Index(name = "must_credential_mobile_number_unique", unique = true, columnList = "credential_mobile_number") })
@SQLDelete(sql = "UPDATE must.credentials SET credential_deleted_at = now() WHERE credential_mobile_number = ? AND credential_updated_at = ?")
public class MustCredential implements Serializable, UserDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "credential_id")
    private long id;

    @OneToOne(mappedBy = "mustCredential")
    private MustProfile mustProfile;

    @Column(name = "credential_password")
    private String password;

    @Column(name = "credential_mobile_number")
    private String mobileNumber;

    @Column(name = "credential_validated_at", nullable = true)
    private Timestamp validatedAt;

    @Column(name = "credential_banned_at", nullable = true)
    private Timestamp bannedAt;

    @Column(name = "credential_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "credential_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "credential_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MustProfile getMustProfile() {
        return mustProfile;
    }

    public void setMustProfile(MustProfile mustProfile) {
        this.mustProfile = mustProfile;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getValidatedAt() {
        return validatedAt;
    }

    public void setValidatedAt(Timestamp validatedAt) {
        this.validatedAt = validatedAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Timestamp getBannedAt() {
        return bannedAt;
    }

    public void setBannedAt(Timestamp bannedAt) {
        this.bannedAt = bannedAt;
    }

    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ROLE_CITIZEN");
    }

    @Override
    @Transient
    public String getUsername() {
        return mobileNumber;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return deletedAt == null;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return bannedAt == null;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return deletedAt == null;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return validatedAt != null;
    }

}
