/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.must;

import com.forsecnet.polri.dashboardback.model.MustReportComment;
import com.forsecnet.polri.dashboardback.model.MustReportCommenter;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author user
 */
@Entity
@Table(name = "comments", schema = "must")
@SQLDelete(sql = "UPDATE must.comments SET comment_deleted_at = now() WHERE comment_id = ? AND comment_updated_at = ?")
public class MustComment implements Serializable, MustReportComment {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8438465899699528121L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "credential_id")
    private MustProfile mustProfile;
    
    @ManyToOne
    @JoinColumn(name = "report_id")
    private MustReport mustReport;
    
    @Column(name = "comment_comment")
    private String comment;
    
    @Column(name = "comment_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;
    
    @Version
    @Column(name = "comment_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "comment_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MustProfile getMustProfile() {
        return mustProfile;
    }

    public void setMustProfile(MustProfile mustProfile) {
        this.mustProfile = mustProfile;
    }

    @Override
    public MustReport getMustReport() {
        return mustReport;
    }

    public void setMustReport(MustReport mustReport) {
        this.mustReport = mustReport;
    }

    @Override
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    @Transient
    public MustReportCommenter getCommenter() {
        return this.mustProfile;
    }
    
    @Override
    @Transient
    public int compareTo(MustReportComment o) {
        return this.createdAt.compareTo(o.getCreatedAt());
    }

}
