/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.ClientDetails;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "clients", schema = "bhabin")
@SQLDelete(sql = "UPDATE bhabin.clients SET client_deleted_at = now() WHERE client_id = ? AND client_updated_at = ?")
public class BhabinClient implements Serializable, ClientDetails {
    
    /**
     * 
     */
    private static final long serialVersionUID = -3193190489514826949L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private long id;
    
    @Column(name = "client_oauth_id")
    private String clientId;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name="clients_resource_id_value")
    @CollectionTable(name="clients_resource_ids", schema = "bhabin", joinColumns = @JoinColumn(name="client_id"))
    private Set<String> resourceIds;
    
    @Column(name = "client_secret_required")
    private boolean secretRequired;
    
    @Column(name = "client_secret")
    private String clientSecret;
    
    @Column(name = "client_scoped")
    private boolean scoped;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name="clients_scope_value")
    @CollectionTable(name="clients_scopes", schema = "bhabin", joinColumns = @JoinColumn(name="client_id"))
    private Set<String> scope;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name="clients_authorized_grant_type_value")
    @CollectionTable(name="clients_authorized_grant_types", schema = "bhabin", joinColumns = @JoinColumn(name="client_id"))
    private Set<String> authorizedGrantTypes;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name="clients_registered_redirect_uri_value")
    @CollectionTable(name="clients_registered_redirect_uris", schema = "bhabin", joinColumns = @JoinColumn(name="client_id"))
    private Set<String> registeredRedirectUri;
    
    @Column(name = "client_auto_approve")
    private boolean autoApprove;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="clients_extra_information_key")
    @Column(name="clients_extra_information_value")
    @CollectionTable(name="clients_extra_informations", schema = "bhabin", joinColumns = @JoinColumn(name="client_id"))
    private Map<String, String> extraInformations;
    
    @Column(name = "client_access_token_validity_seconds")
    private Integer accessTokenValiditySeconds;
    
    @Column(name = "client_refresh_token_validity_seconds")
    private Integer refreshTokenValiditySeconds;
    
    @Column(name = "client_created_at", nullable = false, insertable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;
    
    @Version
    @Column(name = "client_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "client_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(Set<String> resourceIds) {
        this.resourceIds = resourceIds;
    }

    @Override
    public boolean isSecretRequired() {
        return secretRequired;
    }

    public void setSecretRequired(boolean secretRequired) {
        this.secretRequired = secretRequired;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public boolean isScoped() {
        return scoped;
    }

    public void setScoped(boolean scoped) {
        this.scoped = scoped;
    }

    @Override
    public Set<String> getScope() {
        return scope;
    }

    public void setScope(Set<String> scope) {
        this.scope = scope;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return authorizedGrantTypes;
    }

    public void setAuthorizedGrantTypes(Set<String> authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return registeredRedirectUri;
    }

    public void setRegisteredRedirectUri(Set<String> registeredRedirectUri) {
        this.registeredRedirectUri = registeredRedirectUri;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String string) {
        return autoApprove;
    }

    public void setAutoApprove(boolean autoApprove) {
        this.autoApprove = autoApprove;
    }

    public Map<String, String> getExtraInformations() {
        return extraInformations;
    }

    public void setExtraInformations(Map<String, String> extraInformations) {
        this.extraInformations = extraInformations;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    @Transient
    public Collection<GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ROLE_BHABIN");
    }

    @Override
    @Transient
    public Map<String, Object> getAdditionalInformation() {
        return Collections.<String, Object>unmodifiableMap(extraInformations);
    }
    
}
