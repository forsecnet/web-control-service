package com.forsecnet.polri.dashboardback.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "polres", schema = "common")
@SQLDelete(sql = "UPDATE common.polres SET polres_deleted_at = now() WHERE polres_id = ? AND polres_updated_at = ?")
public class Polres implements Serializable {
    


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "polres_id")
    private long id;
    
    @Column(name = "polres_name")
    private String name;
    
    @OneToMany(mappedBy = "polres")
    private List<Polsek> polsek;
    
    @Column(name = "polres_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "polres_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "polres_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Polsek> getPolsek() {
        return polsek;
    }

    public void setPolsek(List<Polsek> polsek) {
        this.polsek = polsek;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
    
    
}
