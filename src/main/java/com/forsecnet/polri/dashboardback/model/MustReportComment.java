/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model;

import com.forsecnet.polri.dashboardback.model.must.MustReport;
import java.sql.Timestamp;

/**
 *
 * @author ahmad
 */
public interface MustReportComment extends Comparable<MustReportComment> {
    public long getId();
    public MustReportCommenter getCommenter();
    public String getComment();
    public MustReport getMustReport();
    public Timestamp getCreatedAt();
}
