/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "devices", schema = "bhabin", indexes = {
    @Index(name = "bhabin_device_key_unique", unique = true, columnList = "device_key")
})
@SQLDelete(sql = "UPDATE bhabin.devices SET device_deleted_at = now() WHERE device_id = ? AND device_updated_at = ?")
public class BhabinDevice implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -3039064756102226339L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_id")
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "credential_id")
    private BhabinProfile owner;
    
    @Column(name = "device_key")
    private String key;
    
    @Column(name = "device_os")
    private String os;
    
    @Column(name = "device_version")
    private String version;
    
    @Column(name = "device_imei")
    private String imei;
    
    @Column(name = "device_brand")
    private String brand;
    
    @Column(name = "device_type")
    private String type;
      
    @Column(name = "device_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;
    
    @Version
    @Column(name = "device_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "device_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BhabinProfile getOwner() {
        return owner;
    }

    public void setOwner(BhabinProfile owner) {
        this.owner = owner;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
    
}
