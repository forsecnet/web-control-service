package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "report_categories", schema = "bhabin")
@SQLDelete(sql = "UPDATE bhabin.report_categories SET report_category_deleted_at = now() WHERE report_category_id = ? AND report_category_updated_at = ?")
public class BhabinReportCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_category_id")
    private long id;
    
    @Column(name = "report_category_name")
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "report_category_master_id")
    private BhabinReportCategory masterCategory;
    
    @OneToMany(mappedBy = "masterCategory")
    private List<BhabinReportCategory> subcategories;
    
    @Column(name = "report_category_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "report_category_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "report_category_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BhabinReportCategory getMasterCategory() {
        return masterCategory;
    }

    public void setMasterCategory(BhabinReportCategory masterCategory) {
        this.masterCategory = masterCategory;
    }

    public List<BhabinReportCategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<BhabinReportCategory> subcategories) {
        this.subcategories = subcategories;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
    
}
