/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.must;

import com.forsecnet.polri.dashboardback.model.MustReportCommenter;
import com.forsecnet.polri.dashboardback.model.TierThreeRegion;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author user
 */
@Entity
@Table(name = "profiles", schema = "must")
@SQLDelete(sql = "UPDATE must.profiles SET profile_deleted_at = now() WHERE credential_id = ? AND profile_updated_at = ?")
public class MustProfile implements Serializable, MustReportCommenter {



    private static final String COMMENTER_ROLE = "MASYARAKAT";

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "mustCredential"))
    @Column(name = "credential_id")
    private long id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "credential_id")
    private MustCredential mustCredential;

    @Column(name = "profile_name")
    private String name;

    @Column(name = "profile_citizen_id")
    private String citizenId;

    @Column(name = "profile_domicile")
    private String domicile;

    @Column(name = "profile_picture_path")
    private String pictureFile;

    @OneToMany(mappedBy = "mustProfile")
    private List<MustPosition> getListPositions;

    @OneToMany(mappedBy = "mustProfile")
    private List<MustComment> getListComments;

    @ManyToOne
    @JoinColumn(name = "tier_three_region_id")
    private TierThreeRegion tierThreeRegion;

    @Column(name = "profile_last_seen_at")
    private Timestamp lastSeenAt;

    @Column(name = "profile_time_offset")
    private long timeOffset;

    @Column(name = "profile_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "profile_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "profile_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MustCredential getMustCredential() {
        return mustCredential;
    }

    public void setMustCredential(MustCredential mustCredential) {
        this.mustCredential = mustCredential;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }

    @Override
    public String getPictureFile() {
        return pictureFile;
    }

    public void setPictureFile(String pictureFile) {
        this.pictureFile = pictureFile;
    }

    public List<MustPosition> getGetListPositions() {
        return getListPositions;
    }

    public void setGetListPositions(List<MustPosition> getListPositions) {
        this.getListPositions = getListPositions;
    }

    public List<MustComment> getGetListComments() {
        return getListComments;
    }

    public void setGetListComments(List<MustComment> getListComments) {
        this.getListComments = getListComments;
    }

    public TierThreeRegion getRegion() {
        return tierThreeRegion;
    }

    public void setRegion(TierThreeRegion tierThreeRegion) {
        this.tierThreeRegion = tierThreeRegion;
    }

    public Timestamp getLastSeenAt() {
        return lastSeenAt;
    }

    public void setLastSeenAt(Timestamp lastSeenAt) {
        this.lastSeenAt = lastSeenAt;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(long timeOffset) {
        this.timeOffset = timeOffset;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String getCommenterRole() {
        return COMMENTER_ROLE;
    }

}
