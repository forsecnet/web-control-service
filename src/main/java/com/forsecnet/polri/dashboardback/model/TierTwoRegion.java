package com.forsecnet.polri.dashboardback.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "tier_two_regions", schema = "common")
@SQLDelete(sql = "UPDATE common.tier_two_regions SET tier_two_region_deleted_at = now() WHERE tier_two_region_id = ? AND tier_two_region_updated_at = ?")
public class TierTwoRegion implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tier_two_region_id")
    private long id;
    
    @Column(name = "tier_two_region_name")
    private String name;
    
    @OneToMany(mappedBy = "tierTwoRegion")
    private List<TierThreeRegion> tierThreeRegions;
    
    @Column(name = "tier_two_region_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "tier_two_region_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "tier_two_region_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TierThreeRegion> getRegions() {
        return tierThreeRegions;
    }

    public void setRegions(List<TierThreeRegion> tierThreeRegions) {
        this.tierThreeRegions = tierThreeRegions;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }
    
    
}
