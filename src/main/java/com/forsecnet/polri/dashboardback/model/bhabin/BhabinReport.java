/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.model.bhabin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLDelete;

/**
 *
 * @author ahmad
 */
@Entity
@Table(name = "reports", schema = "bhabin")
@SQLDelete(sql = "UPDATE bhabin.reports SET report_deleted_at = now() WHERE report_id = ? AND report_updated_at = ?")
public class BhabinReport implements Serializable {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private long id;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "report_picture_file")
    @CollectionTable(name = "report_pictures", schema = "bhabin", joinColumns = @JoinColumn(name = "report_id"))
    @OrderColumn(name = "report_picture_index")
    private List<String> pictureFiles;

    @Column(name = "report_title")
    private String title;

    @Column(name = "report_description")
    private String description;

    @OneToOne
    @JoinColumn(name = "position_id", unique = true)
    private BhabinPosition bhabinPosition;

    @OneToOne
    @JoinColumn(name = "report_category_id")
    private BhabinReportCategory category;

    @Column(name = "report_created_at", nullable = false, insertable = false, updatable = false, columnDefinition = "timestamp without time zone default now()")
    private Timestamp createdAt;

    @Version
    @Column(name = "report_updated_at", nullable = true)
    private Timestamp updatedAt;

    @Column(name = "report_deleted_at", nullable = true)
    private Timestamp deletedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getPictureFiles() {
        return pictureFiles;
    }

    public void setPictureFiles(List<String> pictureFiles) {
        this.pictureFiles = pictureFiles;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BhabinPosition getBhabinPosition() {
        return bhabinPosition;
    }

    public void setBhabinPosition(BhabinPosition bhabinPosition) {
        this.bhabinPosition = bhabinPosition;
    }

    public BhabinReportCategory getCategory() {
        return category;
    }

    public void setCategory(BhabinReportCategory category) {
        this.category = category;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

}
