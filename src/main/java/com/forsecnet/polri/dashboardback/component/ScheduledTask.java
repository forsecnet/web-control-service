/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.component;

import com.forsecnet.polri.dashboardback.model.must.MustCredential;
import com.forsecnet.polri.dashboardback.repository.must.MustCredentialRepository;
import com.forsecnet.polri.dashboardback.service.MustCredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

/**
 *
 * @author user
 */
@Component
public class ScheduledTask {
    
    @Autowired
    MustCredentialService mustCredentialService;

    @Autowired
    MustCredentialRepository mustCredentialRepository;
    
    @Scheduled(fixedDelay = 3600 * 1000 * 2)
    public void unbanMasyarakatListPeriodically() {
        List<MustCredential> bannedMasyarakats = mustCredentialRepository.findAllByBannedAtIsNotNullAndDeletedAtIsNull();
        bannedMasyarakats.forEach(mustCredential -> {
            if(Period.between(mustCredential.getCreatedAt().toLocalDateTime().toLocalDate(), LocalDate.now()).getDays() >= 1){
                mustCredentialService.unbanMasyarakat(mustCredential.getId());
            };
        });

    }
    
}
