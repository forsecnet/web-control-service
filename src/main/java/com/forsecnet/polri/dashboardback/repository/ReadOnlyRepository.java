/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository;

import java.io.Serializable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

/**
 *
 * @author user
 */
@NoRepositoryBean
public interface ReadOnlyRepository<E, PK extends Serializable> extends Repository<E, PK> {
    
    public E findOne(PK id);
    
    public Iterable<E> findAll();
    
}
