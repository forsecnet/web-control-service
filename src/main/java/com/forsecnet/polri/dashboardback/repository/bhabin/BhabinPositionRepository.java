/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.bhabin;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinPosition;
import com.forsecnet.polri.dashboardback.repository.ReadOnlyRepository;

/**
 *
 * @author ahmad
 */
public interface BhabinPositionRepository extends ReadOnlyRepository<BhabinPosition, Long> {
    
}
