/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.must;

import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;
import com.forsecnet.polri.dashboardback.model.must.MustCredential;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author user
 */
@Repository
public interface MustCredentialRepository extends SoftDeleteRepository<MustCredential, Long>{
    
    public MustCredential findOneByMobileNumberAndDeletedAtIsNull(String mobileNumber);
    
    public Page<MustCredential> findAllByMobileNumberAndDeletedAtIsNull(Pageable pageReader);

    public List<MustCredential> findAllByBannedAtIsNotNullAndDeletedAtIsNull();
    
}
