package com.forsecnet.polri.dashboardback.repository;

import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.Polsek;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolsekRepository extends SoftDeleteRepository<Polsek, Long> {

    public Polsek findByNameAndDeletedAtIsNull(String name);

    public List<Polsek> findByPolresAndDeletedAtIsNull(Polres polres);
}
