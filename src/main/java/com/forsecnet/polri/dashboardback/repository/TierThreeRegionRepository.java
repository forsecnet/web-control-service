/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository;

import com.forsecnet.polri.dashboardback.model.TierThreeRegion;
import java.util.List;

/**
 *
 * @author user
 */
public interface TierThreeRegionRepository extends ReadOnlyRepository<TierThreeRegion, Long>{
    
    public List<TierThreeRegion> findTop10ByNameContainsOrderByName(String name);

    public TierThreeRegion findOneByName(String name);
    
}
