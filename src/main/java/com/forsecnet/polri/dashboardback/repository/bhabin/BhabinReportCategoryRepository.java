package com.forsecnet.polri.dashboardback.repository.bhabin;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReportCategory;
import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;

/**
 * Created by BQA on 10/21/16.
 */
public interface BhabinReportCategoryRepository extends SoftDeleteRepository<BhabinReportCategory,Long>{
    public BhabinReportCategory findByNameAndDeletedAtIsNull(String name);
}
