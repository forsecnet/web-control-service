package com.forsecnet.polri.dashboardback.repository;

import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.Polsek;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;


public interface PolresRepository extends SoftDeleteRepository<Polres, Long> {

    public Polres findByNameAndDeletedAtIsNull(String name);

    public List<Polsek> findPolsekByNameAndDeletedAtIsNull(String name);

    public List<Polres> findAllByDeletedAtIsNullOrderByNameAsc();

}
