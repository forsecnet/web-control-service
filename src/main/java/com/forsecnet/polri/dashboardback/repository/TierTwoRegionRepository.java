package com.forsecnet.polri.dashboardback.repository;

import com.forsecnet.polri.dashboardback.model.TierTwoRegion;


public interface TierTwoRegionRepository extends ReadOnlyRepository<TierTwoRegion, Long> {

}
