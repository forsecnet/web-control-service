/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.bhabin;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReportCategory;
import com.forsecnet.polri.dashboardback.repository.ReadOnlyRepository;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;
import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author ahmad
 */
@Repository
public interface BhabinReportRepository extends SoftDeleteRepository<BhabinReport, Long> {
    public Page<BhabinReport> findAllByDeletedAtIsNull(Pageable page);

    public int countByCategoryAndDeletedAtIsNull(BhabinReportCategory category);

    //@Query("SELECT COUNT() FROM BhabinReport as brep JOIN brep.BhabinReportCategory as brepcat JOIN brep.BhabinPosition bpos JOIN bpos.BhabinProfile as bpro JOIN bpro.Polsek as cpols JOIN cpols.Polres as cpolr WHERE brepcat.name = :category AND cpols.name = :polsek")
    @Query(value = "SELECT COUNT(*) FROM bhabin.report_categories, common.polres, common.polsek, bhabin.reports, bhabin.profiles, bhabin.positions WHERE polsek.polres_id = polres.polres_id AND reports.report_category_id = report_categories.report_category_id AND reports.position_id = positions.position_id AND profiles.polsek_id = polsek.polsek_id AND positions.credential_id = profiles.credential_id AND polres.polres_name = ?2 AND report_categories.report_category_name =?1",nativeQuery = true)
    public int countReportByCategory(@Param("category") String category, @Param("polres") String polres);

    public List<BhabinReport> findAllByCreatedAtGreaterThanEqual(Timestamp timestamp);

    public List<BhabinReport> findAllByCreatedAtLessThanEqual(Timestamp timestamp);

}
