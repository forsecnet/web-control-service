/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.must;

import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author user
 */
@Repository
public interface MustReportRepository extends SoftDeleteRepository<MustReport, Long>{

    public Page<MustReport> findAllByDeletedAtIsNull(Pageable page);

    public Page<MustReport> countAllByDeletedAtIsNull(Pageable page);

    public Page<MustReport> findAllByDeletedAtIsNotNull(Pageable page);

    public Page<MustReport> findAllByRespondedAtIsNotNullAndCompletedAtIsNotNullAndDeletedAtIsNull(Pageable page);


    public int countByCategory(MustReport.Category category);

    @Query("SELECT mr FROM MustReport mr JOIN mr.mustPosition pos JOIN pos.mustProfile mp WHERE mr.id = :id AND mp.id = :mustProfileId")
    public MustReport findOneInContext(@Param("id") long id, @Param("mustProfileId") long mustProfileId);

    @Query("SELECT mr FROM MustReport mr JOIN mr.mustPosition pos JOIN pos.mustProfile mp WHERE mp.id = :mustProfileId")
    public Page<MustReport> findAllInContext(Pageable page, @Param("mustProfileId") long mustProfileId);

    @Query(value = "SELECT EXTRACT(EPOCH FROM AVG(must.reports.report_completed_at - must.reports.report_created_at)) FROM must.reports, bhabin.profiles, common.polsek, common.polres WHERE reports.handler_id = profiles.credential_id AND profiles.polsek_id = polsek.polsek_id AND polsek.polres_id = polres.polres_id AND polres.polres_name = ?1", nativeQuery = true)
    public double findAverageByPolresName(@Param("name") String polres);

    @Query(value = "SELECT COUNT(*) FROM bhabin.profiles, common.polsek, common.polres, must.reports WHERE profiles.polsek_id = polsek.polsek_id AND polsek.polres_id = polres.polres_id AND reports.handler_id = profiles.credential_id AND polres.polres_name = ?1 AND reports.report_category = ?2", nativeQuery = true)
    public long countReportCountByPolresAndCategory(@Param("polres") String polres, @Param("category") String category);

    public List<MustReport> findAllByCategoryAndDeletedAtIsNull(MustReport.Category category);
    
}
