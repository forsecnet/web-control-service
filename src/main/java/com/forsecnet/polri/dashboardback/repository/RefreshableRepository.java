package com.forsecnet.polri.dashboardback.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface RefreshableRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
    public void refresh(T entity);
}
