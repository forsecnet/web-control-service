/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.must;

import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;
import com.forsecnet.polri.dashboardback.model.must.MustProfile;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

/**
 *
 * @author user
 */
@Repository
public interface MustProfileRepository extends SoftDeleteRepository<MustProfile, Long> {


}
