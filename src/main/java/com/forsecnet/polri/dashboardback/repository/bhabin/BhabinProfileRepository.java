/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.repository.bhabin;

import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.ReadOnlyRepository;
import com.forsecnet.polri.dashboardback.model.TierThreeRegion;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;
import com.forsecnet.polri.dashboardback.repository.SoftDeleteRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author ahmad
 */
@Repository
public interface BhabinProfileRepository extends SoftDeleteRepository<BhabinProfile, Long> {

    public BhabinProfile findOneByBhabinCredentialNrpAndDeletedAtIsNull(String nrp);

    public List<BhabinProfile> findAllByTierThreeRegionsAndDeletedAtIsNull(TierThreeRegion region);

    public List<BhabinProfile> findAllByDeletedAtIsNull();

    public List<BhabinProfile> countAllByDeletedAtIsNull();

}
