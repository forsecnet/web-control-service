package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.ReportRecap2Dto;
import com.forsecnet.polri.dashboardback.dto.ReportRecapDto;
import com.forsecnet.polri.dashboardback.dto.ReportRecapSumDto;
import com.forsecnet.polri.dashboardback.dto.ReportRecapSumOnlyDto;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinReportRepository;
import org.springframework.data.domain.PageRequest;

/**
 * Created by BQA on 10/21/16.
 */
public interface ReportRecapService extends DtoService<ReportRecapDto, BhabinReport, Long, BhabinReportRepository>{

    public ReportRecapDto getReportRecapAndConvertToDto(PageRequest pageRequest,String category) throws ReportException;

    public ReportRecap2Dto getReportRecapByPolresAndConvertToDto(PageRequest pageRequest, String category) throws ReportException;

    public ReportRecapSumDto getReportRecapSumOnly(PageRequest pageRequest, String category) throws ReportException;

}
