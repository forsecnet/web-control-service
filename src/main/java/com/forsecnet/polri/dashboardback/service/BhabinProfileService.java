package com.forsecnet.polri.dashboardback.service;


import com.forsecnet.polri.dashboardback.dto.BhabinPointDto;
import com.forsecnet.polri.dashboardback.dto.BhabinProfileDto;
import com.forsecnet.polri.dashboardback.dto.BhabinRankDto;
import com.forsecnet.polri.dashboardback.dto.PolresDto;
import com.forsecnet.polri.dashboardback.exception.ProfileException;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinProfileRepository;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by BQA on 10/21/16.
 */
public interface BhabinProfileService extends DtoService<BhabinProfileDto, BhabinProfile, Long, BhabinProfileRepository>{


    public BhabinProfileDto getAndConvertToDTO(Long id) throws ReportException;

    public List<BhabinProfileDto> listAndConvertToDTO(PageRequest pageRequest) throws ReportException;

    public BhabinPointDto getPoint(long mustId);

    public List<BhabinRankDto> getRanking(int limit, int offset);

    public BhabinProfileDto registerUser(
            String name,
            String nrp,
            String rank,
            String telepon,
            String password,
            String alamat,
            String polsek,
            String polres,
            String location
    ) throws ProfileException;

    public BhabinProfileDto updateUser(
            long id,
            String name,
            String rank,
            String telepon,
            String password,
            String alamat,
            String polsek,
            String polres,
            String pictureFilePath,
            String location
    ) throws ProfileException;

    public List<PolresDto> listPolresWithCorrespondingPolsekAndConvertToDto(PageRequest pageRequest);

    public BhabinProfileDto deleteBhabinAndConvertToDto(long id);

}
