package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.MustCredentialDto;
import com.forsecnet.polri.dashboardback.dto.MustProfileDto;
import com.forsecnet.polri.dashboardback.dto.RejectedReportDto;
import com.forsecnet.polri.dashboardback.model.must.MustCredential;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.must.MustCredentialRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustProfileRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BQA on 10/25/16.
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class MustCredentialServiceImpl extends DtoServiceImpl<MustCredentialDto, MustCredential, Long, MustCredentialRepository> implements MustCredentialService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Autowired
    public void setJpaRepository(MustCredentialRepository mustCredentialRepository) {
        super.setJpaRepository(mustCredentialRepository);
    }

    @Autowired
    MustCredentialRepository mustCredentialRepository;

    @Autowired
    MustReportRepository mustReportRepository;

    @Override
    public List<RejectedReportDto> getRejectedReportsOwnerProfileList(PageRequest pageRequest) {
        List<MustReport> rejectedReportsList = mustReportRepository.findAllByDeletedAtIsNotNull(pageRequest).getContent();
        List<RejectedReportDto> rejectedReportDtos = new ArrayList<>();
        rejectedReportsList.forEach(mustReport -> {
            rejectedReportDtos.add(new RejectedReportDto(mustReport));
        });
        return rejectedReportDtos;
    }

    @Override
    public MustCredentialDto banMasyarakatsAndConverToDto(long credentialId) {
        MustCredential mustCredential = mustCredentialRepository.findOneByIdAndDeletedAtIsNull(credentialId);
        mustCredential.setBannedAt(Timestamp.from(Instant.now()));
        mustCredential.setUpdatedAt(Timestamp.from(Instant.now()));
        update(mustCredential);
        return new MustCredentialDto(mustCredential);

    }

    @Override
    public void unbanMasyarakat(long credentialId) {
        MustCredential mustCredential = mustCredentialRepository.findOneByIdAndDeletedAtIsNull(credentialId);
        mustCredential.setBannedAt(null);
        mustCredential.setUpdatedAt(Timestamp.from(Instant.now()));
        update(mustCredential);
    }

    @Override
    public List<MustCredentialDto> getListBannedMasyarakatAndConvertToDto() {
        List<MustCredential> mustCredentialList = mustCredentialRepository.findAllByBannedAtIsNotNullAndDeletedAtIsNull();
        List<MustCredentialDto> mustCredentialDtoList = new ArrayList<>();
        mustCredentialList.forEach(mustCredential -> {
            mustCredentialDtoList.add(new MustCredentialDto(mustCredential));
        });
        return mustCredentialDtoList;
    }
}
