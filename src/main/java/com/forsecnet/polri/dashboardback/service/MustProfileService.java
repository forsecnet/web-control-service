/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.*;
import com.forsecnet.polri.dashboardback.model.must.MustProfile;
import com.forsecnet.polri.dashboardback.repository.must.MustProfileRepository;

import java.util.List;

/**
 *
 * @author user
 */
public interface MustProfileService extends DtoService<MustProfileDto, MustProfile, Long, MustProfileRepository>{

    
    public PointDto getPoint(long mustId);
    
    public List<RankingDto> getRanking(int limit, int offset);
    
    public void refreshRanking();
    
    public void noticePresent(long id);
    
    public List<RegionDto> getRegionContainAndConvertToDTO(String name);

    public MustProfileDto getMasyarakatWithPointsAndConvertToDto(long id);

    
}
