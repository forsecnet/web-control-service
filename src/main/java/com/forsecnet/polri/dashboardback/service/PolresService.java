package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.AverageResponseDto;
import com.forsecnet.polri.dashboardback.dto.PolresDto;
import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by BQA on 10/25/16.
 */
public interface PolresService extends DtoService<PolresDto, Polres, Long, PolresRepository> {

    public List<AverageResponseDto> getAverageByResponseTimeList(Pageable pageRequest);
}
