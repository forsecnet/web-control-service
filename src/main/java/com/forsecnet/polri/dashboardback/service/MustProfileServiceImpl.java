/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.*;
import com.forsecnet.polri.dashboardback.model.must.MustCredential;
import com.forsecnet.polri.dashboardback.model.must.MustProfile;
import com.forsecnet.polri.dashboardback.repository.must.MustCredentialRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustPositionRepository;

import javax.transaction.Transactional;

import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.forsecnet.polri.dashboardback.repository.must.MustProfileRepository;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.stream.Collectors;

import com.forsecnet.polri.dashboardback.repository.TierThreeRegionRepository;

@Service
@Transactional(Transactional.TxType.REQUIRED)
public class MustProfileServiceImpl extends DtoServiceImpl<MustProfileDto, MustProfile, Long, MustProfileRepository> implements MustProfileService {

    private static final Logger LOGGER = Logger.getLogger(MustProfileServiceImpl.class);

    @Autowired
    private MustProfileRepository mustProfileRepository;

    @Autowired
    private MustCredentialRepository mustCredentialRepository;

    @Autowired
    private TierThreeRegionRepository regionRepository;

    @Autowired
    private MustPositionRepository mustPositionRepository;

    @Autowired
    private MustReportRepository mustReportRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${ebhabin.must.path.base}")
    private String baseUrl;

    @Value("${ebhabin.must.path.picture.profile}")
    private String pictureUrl;

    @Value("${ebhabin.must.store.picture.profile}")
    private String pictureStore;

    @Override
    @Autowired
    public void setJpaRepository(MustProfileRepository mustProfileRepository) {
        super.setJpaRepository(mustProfileRepository);
    }


    @Override
    public MustProfileDto getAndConvertToDTO(Long id) {
        MustProfile mustProfile = mustCredentialRepository.findOneByIdAndDeletedAtIsNull(id).getMustProfile();

        MustProfileDto mustProfileDto = new MustProfileDto(mustProfile, getPoint(id));
        mustProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + mustProfileDto.getPicturePath());

        return mustProfileDto;
    }

    @Override
    public MustProfile get(Long id) {
        return mustCredentialRepository.findOneByIdAndDeletedAtIsNull(id).getMustProfile();
    }

    @Override
    public List<MustProfileDto> listAndConvertToDTO(PageRequest pageRequest) {
        List<MustCredential> listMustCredential = mustCredentialRepository.findAllByDeletedAtIsNull(pageRequest).getContent();
        ArrayList<MustProfileDto> profilesDTO = new ArrayList<>();

        listMustCredential.stream().map((mustCredential) -> {
            MustProfileDto mustProfileDto = new MustProfileDto(mustCredential.getMustProfile(), getPoint(mustCredential.getId()));
            mustProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + mustProfileDto.getPicturePath());
            return mustProfileDto;
        }).forEach((mustProfileDTO) -> {
            profilesDTO.add(mustProfileDTO);
        });

        return profilesDTO;
    }

    @Override
    public List<MustProfile> list(PageRequest pageRequest) {
        List<MustCredential> listMustCredential = mustCredentialRepository.findAllByMobileNumberAndDeletedAtIsNull(pageRequest).getContent();
        ArrayList<MustProfile> listMustProfiles = new ArrayList<>();

        listMustCredential.stream().forEach((mustCredential) -> {
            listMustProfiles.add(mustCredential.getMustProfile());
        });

        return listMustProfiles;
    }

    @Override
    public PointDto getPoint(long mustId) {
        String query = "SELECT * FROM must.rankings r WHERE r.credential_id = :id";

        Query q = entityManager.createNativeQuery(query);
        q.setParameter("id", mustId);
        try {
            Object[] result = (Object[]) q.getSingleResult();

            PointDto pointDTO = new PointDto();

            pointDTO.setRank(((BigInteger) result[0]).longValue());
            pointDTO.setMerah(((BigInteger) result[4]).longValue());
            pointDTO.setKuning(((BigInteger) result[5]).longValue());
            pointDTO.setHijau(((BigInteger) result[6]).longValue());
            pointDTO.setTotal(((BigInteger) result[7]).longValue());
            pointDTO.setRefreshedAt(((Timestamp) result[8]).getTime());

            return pointDTO;
        } catch (NoResultException ex) {
            LOGGER.error("No result exception", ex);
            PointDto pointDTO = new PointDto();

            pointDTO.setRank(0);
            pointDTO.setMerah(0);
            pointDTO.setKuning(0);
            pointDTO.setHijau(0);
            pointDTO.setTotal(0);
            pointDTO.setRefreshedAt(Instant.now().getEpochSecond());

            return pointDTO;
        }
    }

    @SuppressWarnings("JpaQueryApiInspection")
    @Override
    public List<RankingDto> getRanking(int limit, int offset) {
        String query = "SELECT * FROM must.rankings r ORDER BY r.rank LIMIT :limit OFFSET :offset";

        Query q = entityManager.createNativeQuery(query);
        q.setParameter("limit", limit);
        q.setParameter("offset", offset);

        List<Object[]> results = (List<Object[]>) q.getResultList();
        ArrayList<RankingDto> listRanking = new ArrayList<>();

        results.stream().map((rowResult) -> {
            RankingDto rankingDTO = new RankingDto();
            rankingDTO.setRank(((BigInteger) rowResult[0]).longValue());
            rankingDTO.setId(((BigInteger) rowResult[1]).longValue());
            rankingDTO.setName((String) rowResult[2]);
            rankingDTO.setLokasi((String) rowResult[3]);
            rankingDTO.setMerah(((BigInteger) rowResult[4]).longValue());
            rankingDTO.setKuning(((BigInteger) rowResult[5]).longValue());
            rankingDTO.setHijau(((BigInteger) rowResult[6]).longValue());
            rankingDTO.setTotal(((BigInteger) rowResult[7]).longValue());
            rankingDTO.setRefreshedAt(((Timestamp) rowResult[8]).getTime());
            return rankingDTO;
        }).forEach((rankingDTO) -> {
            listRanking.add(rankingDTO);
        });

        return listRanking;
    }

    @Override
    public void refreshRanking() {
        String query
                = "SELECT * FROM must.refresh_rankings()";

        Object result = entityManager.createNativeQuery(query).getSingleResult();
        LOGGER.debug("Ranking refreshed: current version is " + ((Timestamp) result).getTime());
    }

    @Override
    public void noticePresent(long id) {
        MustProfile mustProfile = this.jpaRepository.findOne(id);
        mustProfile.setLastSeenAt(Timestamp.from(Instant.now()));
        update(mustProfile);
    }

    @Override
    public List<RegionDto> getRegionContainAndConvertToDTO(String name) {
        return regionRepository.findTop10ByNameContainsOrderByName(name).stream().map(region -> {
            RegionDto regionDto = new RegionDto(region);
            return regionDto;
        }).collect(Collectors.toList());
    }

    @Override
    public MustProfileDto getMasyarakatWithPointsAndConvertToDto(long id) {
        MustProfile mustProfile = mustProfileRepository.findOneByIdAndDeletedAtIsNull(id);
        MustProfileDto mustProfileDto= new MustProfileDto(mustProfile,this.getPoint(id));
        return mustProfileDto;
    }


}
