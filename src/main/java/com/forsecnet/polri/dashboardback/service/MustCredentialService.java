package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.MustCredentialDto;
import com.forsecnet.polri.dashboardback.dto.MustProfileDto;
import com.forsecnet.polri.dashboardback.dto.RejectedReportDto;
import com.forsecnet.polri.dashboardback.model.must.MustCredential;
import com.forsecnet.polri.dashboardback.repository.must.MustCredentialRepository;
import org.springframework.data.domain.PageRequest;

import java.time.Instant;
import java.time.Period;
import java.util.List;

/**
 * Created by BQA on 10/25/16.
 */
public interface MustCredentialService extends DtoService<MustCredentialDto, MustCredential, Long, MustCredentialRepository> {

    public List<RejectedReportDto> getRejectedReportsOwnerProfileList(PageRequest pageRequest);

    public MustCredentialDto banMasyarakatsAndConverToDto(long credentialId);

    public void unbanMasyarakat(long credentialId);

    public List<MustCredentialDto> getListBannedMasyarakatAndConvertToDto();

}
