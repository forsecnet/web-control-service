/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.BaseDto;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author user
 */
public interface DtoService<D extends BaseDto<E>, E, PK extends Serializable, R extends JpaRepository<E, PK>> extends BaseService<E, PK, R> {
    
    public D getAndConvertToDTO(PK id);
    
    public List<D> listAndConvertToDTO(PageRequest pageRequest);
    
}
