/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.LaporanDto;
import com.forsecnet.polri.dashboardback.dto.MustReportCountCategoryDto;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import java.awt.image.BufferedImage;
import java.util.List;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author user
 */
public interface MustReportService extends DtoService<LaporanDto, MustReport, Long, MustReportRepository>{
    
    public LaporanDto getAndConvertToDTO(Long id, Long mustProfileId) throws ReportException;
    
    public List<LaporanDto> listAndConvertToDTO(PageRequest pageRequest, Long mustProfileId) throws ReportException;

    public MustReportCountCategoryDto listReportByCategoryAndPolresAndConvertToDto(String polres) throws ReportException;
}
