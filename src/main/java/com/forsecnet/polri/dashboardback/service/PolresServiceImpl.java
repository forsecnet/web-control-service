package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.AverageResponseDto;
import com.forsecnet.polri.dashboardback.dto.PolresDto;
import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.Polsek;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import com.forsecnet.polri.dashboardback.repository.PolsekRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by BQA on 10/25/16.
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class PolresServiceImpl extends DtoServiceImpl<PolresDto, Polres, Long, PolresRepository> implements PolresService {

    @Autowired
    PolresRepository polresRepository;

    @Autowired
    PolsekRepository polsekRepository;

    @Autowired
    MustReportRepository mustReportRepository;

    @Override
    public List<PolresDto> listAndConvertToDTO(PageRequest pageRequest) {
        List<PolresDto> polresList = new ArrayList();
        List<Polres> polreses = polresRepository.findAllByDeletedAtIsNull(pageRequest).getContent();
        polreses.forEach(polres -> {
            List<Polsek> polseks = polsekRepository.findByPolresAndDeletedAtIsNull(polres);
            PolresDto polresDto = new PolresDto(polres, polseks);
            polresList.add(polresDto);
        });
        return polresList;
    }

    @Override
    public List<AverageResponseDto> getAverageByResponseTimeList(Pageable pageRequest) {
        List<MustReport> mustReportList = mustReportRepository.findAllByRespondedAtIsNotNullAndCompletedAtIsNotNullAndDeletedAtIsNull(pageRequest).getContent();
        HashSet<Polres> polresList = new HashSet<>();
        List<AverageResponseDto> averageResponseDtoList = new ArrayList<>();
        mustReportList.forEach(mustReport -> {
            polresList.add(mustReport.getHandler().getPolsek().getPolres());
        });
        polresList.size();
        polresList.forEach(polres -> {
            averageResponseDtoList.add(new AverageResponseDto(polres, mustReportRepository.findAverageByPolresName(polres.getName())));
        });
        averageResponseDtoList.sort(Comparator.comparing(AverageResponseDto::getAverage));
        return averageResponseDtoList;
    }
}
