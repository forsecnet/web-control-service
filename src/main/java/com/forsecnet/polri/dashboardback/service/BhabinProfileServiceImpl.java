package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.*;
import com.forsecnet.polri.dashboardback.exception.ProfileException;
import com.forsecnet.polri.dashboardback.exception.ProfileProcessingException;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.Polsek;
import com.forsecnet.polri.dashboardback.model.TierThreeRegion;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinCredential;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import com.forsecnet.polri.dashboardback.repository.PolsekRepository;
import com.forsecnet.polri.dashboardback.repository.TierThreeRegionRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinCredentialRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinPositionRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinProfileRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * Created by BQA on 10/21/16.
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class BhabinProfileServiceImpl extends DtoServiceImpl<BhabinProfileDto, BhabinProfile, Long, BhabinProfileRepository> implements BhabinProfileService {

    private static final Logger LOGGER = Logger.getLogger(MustProfileServiceImpl.class);


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    TierThreeRegionRepository regionRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    BhabinCredentialRepository bhabinCredentialRepository;

    @Autowired
    BhabinPositionRepository bhabinPositionRepository;

    @Autowired
    PolsekRepository polsekRepository;

    @Autowired
    PolresRepository polresRepository;

    @Autowired
    BhabinProfileRepository bhabinProfileRepository;

    @Value("${ebhabin.must.path.base}")
    private String baseUrl;

    @Value("${ebhabin.must.path.picture.profile}")
    private String pictureUrl;

    @Value("${ebhabin.must.store.picture.profile}")
    private String pictureStore;

    @Override
    @Autowired
    public void setJpaRepository(BhabinProfileRepository bhabinProfileRepository) {
        super.setJpaRepository(bhabinProfileRepository);
    }

    @Override
    public BhabinProfileDto getAndConvertToDTO(Long id) throws ReportException {
        BhabinProfile bhabinProfile = bhabinCredentialRepository.findOneByIdAndDeletedAtIsNull(id).getBhabinProfile();

        if(bhabinProfile == null){
            throw new ProfileProcessingException("No Bhabin With That Ids Available");
        }

        BhabinProfileDto bhabinProfileDto = new BhabinProfileDto(bhabinProfile);
        bhabinProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + bhabinProfileDto.getPicturePath());

        return bhabinProfileDto;
    }

    @Override
    public List<BhabinProfileDto> listAndConvertToDTO(PageRequest pageRequest) {
        List<BhabinCredential> listMustCredential = bhabinCredentialRepository.findAllByDeletedAtIsNull(pageRequest).getContent();
        ArrayList<BhabinProfileDto> profilesDTO = new ArrayList<>();

        listMustCredential.stream().map((bhabinCredential) -> {
            BhabinProfileDto bhabinProfileDto = new BhabinProfileDto(bhabinCredential.getBhabinProfile());
            bhabinProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + bhabinProfileDto.getPicturePath());
            return bhabinProfileDto;
        }).forEach((bhabinProfileDTO) -> {
            profilesDTO.add(bhabinProfileDTO);
        });

        return profilesDTO;
    }

    @Override
    public BhabinProfileDto registerUser(
            String name,
            String nrp,
            String rank,
            String telepon,
            String password,
            String alamat,
            String polsek,
            String polres,
            String location
    ) throws ProfileException {
        Polres polresCheck = polresRepository.findByNameAndDeletedAtIsNull(polres);
        List<Polsek> polsekCheck = polsekRepository.findByPolresAndDeletedAtIsNull(polresCheck);

        if (bhabinCredentialRepository.findOneByNrpAndDeletedAtIsNull(nrp) != null) {
            throw new ProfileProcessingException("Register fail or user already exists");
        }

        if (polresCheck == null) {
            throw new ProfileProcessingException("No Polres Available");
        }

        if (polsekCheck == null) {
            throw new ProfileProcessingException("No Suitable Polsek for Specified Polres");
        }

        if (regionRepository.findOneByName(location) == null || regionRepository.findAll() == null) {
            throw new ProfileProcessingException("No Matching Location on Our Database");
        }

        BhabinCredential bhabinCredential = new BhabinCredential();
        bhabinCredential.setNrp(nrp);
        bhabinCredential.setPassword(passwordEncoder.encode(password));
        bhabinCredential.setValidatedAt(Timestamp.from(Instant.now()));

        BhabinCredential savedCredential = bhabinCredentialRepository.save(bhabinCredential);
        List<TierThreeRegion> bhabinRegion = Arrays.asList(regionRepository.findOneByName(location));

        BhabinProfile bhabinProfile = new BhabinProfile();
        bhabinProfile.setName(name);
        bhabinProfile.setPolsek(polsekRepository.findByNameAndDeletedAtIsNull(polsek));
        bhabinProfile.setRank(BhabinProfile.Rank.valueOf(rank));
        bhabinProfile.setPhone(telepon);
        bhabinProfile.setPictureFile("default.png");
        bhabinProfile.setHomeAddress(alamat);
        bhabinProfile.setBhabinCredential(savedCredential);
        bhabinProfile.setTierThreeRegions(bhabinRegion);

        BhabinProfile savedProfile = bhabinProfileRepository.save(bhabinProfile);

        BhabinProfileDto bhabinProfileDto = new BhabinProfileDto(savedProfile);
        bhabinProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + bhabinProfileDto.getPicturePath());

        return bhabinProfileDto;

    }

    @Override
    public BhabinProfileDto updateUser(
            long id,
            String name,
            String rank,
            String telepon,
            String password,
            String alamat,
            String polsek,
            String polres,
            String pictureFilePath,
            String location
    ) throws ProfileException {
        Polres polresCheck = polresRepository.findByNameAndDeletedAtIsNull(polres);
        List<Polsek> polsekCheck = polsekRepository.findByPolresAndDeletedAtIsNull(polresCheck);

        if (bhabinCredentialRepository.findByIdAndDeletedAtIsNull(id) == null) {
            throw new ProfileProcessingException("No Bhabin Exist in Database");
        }

        if (polresCheck == null) {
            throw new ProfileProcessingException("No Polres Available");
        }

        if (polsekCheck == null) {
            throw new ProfileProcessingException("No Suitable Polsek for Specified Polres");
        }

        if (regionRepository.findOneByName(location) == null || regionRepository.findAll() == null) {
            throw new ProfileProcessingException("No Matching Location on Our Database");
        }

        BhabinCredential bhabinCredential = bhabinCredentialRepository.findByIdAndDeletedAtIsNull(id);
        BhabinProfile bhabinProfile = bhabinCredential.getBhabinProfile();
        List<TierThreeRegion> bhabinRegion = new ArrayList<>();
        bhabinRegion.add(regionRepository.findOneByName(location));

        bhabinCredential.setPassword(passwordEncoder.encode(password));
        bhabinCredential.setUpdatedAt(Timestamp.from(Instant.now()));
        //BhabinCredential savedCredential = bhabinCredentialRepository.save(bhabinCredential);

        bhabinProfile.setPhone(telepon);
        bhabinProfile.setName(name);
        bhabinProfile.setHomeAddress(alamat);
        bhabinProfile.setPictureFile(pictureFilePath);
        bhabinProfile.setRank(BhabinProfile.Rank.valueOf(rank));
        bhabinProfile.setUpdatedAt(Timestamp.from(Instant.now()));
        bhabinProfile.setBhabinCredential(bhabinCredential);
        bhabinProfile.setTierThreeRegions(bhabinRegion);
        bhabinProfile.setPolsek(polsekRepository.findByNameAndDeletedAtIsNull(polsek));

        BhabinProfileDto nyaaa = new BhabinProfileDto();
        BhabinProfile sadProfile = this.update(bhabinProfile);
        BhabinProfileDto bhabinProfileDto = new BhabinProfileDto(sadProfile);
        bhabinProfileDto.setPicturePath(baseUrl + pictureUrl + "/" + bhabinProfileDto.getPicturePath());

        return bhabinProfileDto;

    }

    @Override
    public List<PolresDto> listPolresWithCorrespondingPolsekAndConvertToDto(PageRequest pageRequest) {
        List<PolresDto> polresList = new ArrayList();
        List<Polres> polreses = polresRepository.findAllByDeletedAtIsNull(pageRequest).getContent();
        polreses.forEach(polres -> {
            List<Polsek> polseks = polsekRepository.findByPolresAndDeletedAtIsNull(polres);
            PolresDto polresDto = new PolresDto(polres, polseks);
            polresList.add(polresDto);
        });
        return polresList;
    }

    @Override
    public BhabinProfileDto deleteBhabinAndConvertToDto(long id) {
        BhabinProfile bhabinProfile = bhabinProfileRepository.findOneByIdAndDeletedAtIsNull(id);
        bhabinProfile.setDeletedAt(Timestamp.from(Instant.now()));
        return new BhabinProfileDto(bhabinProfile);
    }

    @Override
    public BhabinPointDto getPoint(long mustId) {
        String query = "SELECT * FROM bhabin.rankings r WHERE r.credential_id = :id";

        Query q = entityManager.createNativeQuery(query);
        q.setParameter("id", mustId);
        try {
            Object[] result = (Object[]) q.getSingleResult();

            BhabinPointDto pointDTO = new BhabinPointDto();

            pointDTO.setRank(((BigInteger) result[0]).longValue());
            pointDTO.setMustPoint(((BigInteger) result[7]).longValue());
            pointDTO.setDds(((BigInteger) result[8]).longValue());
            pointDTO.setProblemSolving(((BigInteger) result[9]).longValue());
            pointDTO.setGiatKreatif(((BigInteger) result[10]).longValue());
            pointDTO.setBhabinPoint(((BigInteger) result[11]).longValue());
            pointDTO.setTotal(((BigInteger) result[12]).longValue());
            pointDTO.setRefreshedAt(((Timestamp) result[13]).getTime());

            return pointDTO;
        } catch (NoResultException ex) {
            LOGGER.error("No result exception", ex);
            BhabinPointDto pointDTO = new BhabinPointDto();

            pointDTO.setRank(0);
            pointDTO.setMustPoint(0);
            pointDTO.setDds(0);
            pointDTO.setProblemSolving(0);
            pointDTO.setGiatKreatif(0);
            pointDTO.setBhabinPoint(0);
            pointDTO.setTotal(0);
            pointDTO.setRefreshedAt(Instant.now().getEpochSecond());

            return pointDTO;
        }
    }

    @SuppressWarnings("JpaQueryApiInspection")
    @Override
    public List<BhabinRankDto> getRanking(int limit, int offset) {
        String query = "SELECT * FROM bhabin.rankings r ORDER BY r.rank LIMIT :limit OFFSET :offset";

        Query q = entityManager.createNativeQuery(query);
        q.setParameter("limit", limit);
        q.setParameter("offset", offset);

        List<Object[]> results = (List<Object[]>) q.getResultList();
        ArrayList<BhabinRankDto> listRanking = new ArrayList<>();

        results.stream().map((rowResult) -> {
            BhabinRankDto rankingDTO = new BhabinRankDto();
            rankingDTO.setRanking(((BigInteger) rowResult[0]).longValue());
            rankingDTO.setId(((BigInteger) rowResult[1]).longValue());
            rankingDTO.setName((String) rowResult[3]);
            rankingDTO.setMustPoint(((BigInteger) rowResult[7]).longValue());
            rankingDTO.setDdsPoint(((BigInteger) rowResult[8]).longValue());
            rankingDTO.setProblemSolvingPoint(((BigInteger) rowResult[9]).longValue());
            rankingDTO.setGiatKreatifPoint(((BigInteger) rowResult[10]).longValue());
            rankingDTO.setBhabinPoint(((BigInteger) rowResult[11]).longValue());
            rankingDTO.setTotal(((BigInteger) rowResult[12]).longValue());
            rankingDTO.setRefreshedAt(((Timestamp) rowResult[13]).getTime());
            return rankingDTO;
        }).forEach((rankingDTO) -> {
            listRanking.add(rankingDTO);
        });

        return listRanking;
    }
}
