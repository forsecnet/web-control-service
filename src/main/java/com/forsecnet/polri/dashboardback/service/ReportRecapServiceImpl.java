package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.*;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;
import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReportCategory;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import com.forsecnet.polri.dashboardback.repository.PolsekRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinReportCategoryRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by BQA on 10/21/16.
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class ReportRecapServiceImpl extends DtoServiceImpl<ReportRecapDto, BhabinReport, Long, BhabinReportRepository> implements ReportRecapService {

    @Autowired
    BhabinReportRepository bhabinReportRepository;

    @Autowired
    BhabinReportCategoryRepository bhabinReportCategoryRepository;

    @Autowired
    PolsekRepository polsekRepository;

    @Autowired
    PolresRepository polresRepository;

    @Override
    @Autowired
    public void setJpaRepository(BhabinReportRepository bhabinReportRepository) {
        super.setJpaRepository(bhabinReportRepository);
    }

    @Override
    public ReportRecapDto getReportRecapAndConvertToDto(PageRequest pageRequest, String category) throws ReportException {
        if (bhabinReportCategoryRepository.findAllByDeletedAtIsNull(pageRequest) == null) {
            throw new ReportException("No Category to be Processed");
        }

        BhabinReportCategory reportCategory = bhabinReportCategoryRepository.findByNameAndDeletedAtIsNull(category);
        List<Polres> polresList = polresRepository.findAllByDeletedAtIsNullOrderByNameAsc();
        ReportRecapDto recap = new ReportRecapDto();
        List<RecapSubCategoryDto> recapSubCategoryDtoArrayList = new ArrayList<>();
        List<BhabinReportCategory> listSubCategory = new ArrayList<>();
        List<Integer> sumList = new ArrayList<>();


        if (reportCategory.getSubcategories().size() > 0) {
            listSubCategory = reportCategory.getSubcategories();
        } else if (reportCategory.getSubcategories().size() == 0) {
            listSubCategory.add(reportCategory);
        }
        recap.setCategory(category);

        listSubCategory.forEach(subCategory -> {
            Map<String, Integer> asd = new HashMap<String, Integer>();
            polresList.forEach(polres -> {
                int val = bhabinReportRepository.countReportByCategory(subCategory.getName(), polres.getName());
                asd.put(polres.getName(), val);
                sumList.add(val);
            });
            recapSubCategoryDtoArrayList.add(new RecapSubCategoryDto(subCategory.getName(), asd));
        });
        int total = sumList.stream().mapToInt(Integer::intValue).sum();
        recap.setTotal(total);
        recap.setDetails(recapSubCategoryDtoArrayList);
        return recap;


    }

    @Override
    public ReportRecap2Dto getReportRecapByPolresAndConvertToDto(PageRequest pageRequest, String category) throws ReportException {
        if (bhabinReportCategoryRepository.findAllByDeletedAtIsNull(pageRequest) == null) {
            throw new ReportException("No Category to be Processed");
        }

        BhabinReportCategory reportCategory = bhabinReportCategoryRepository.findByNameAndDeletedAtIsNull(category);
        List<Polres> polresList = polresRepository.findAllByDeletedAtIsNullOrderByNameAsc();
        ReportRecap2Dto recap = new ReportRecap2Dto();
        List<RecapSumOnlyDto> recapSubCategoryDtoArrayList = new ArrayList<>();
        final List<BhabinReportCategory> listSubCategory = new ArrayList<>();
        List<Integer> sumList = new ArrayList<>();


        if (reportCategory.getSubcategories().size() > 0) {
            reportCategory.getSubcategories().forEach(category1 -> {
                listSubCategory.add(category1);
            });
        } else if (reportCategory.getSubcategories().size() == 0) {
            listSubCategory.add(reportCategory);
        }
        recap.setCategory(category);

        polresList.forEach(polres -> {
            Map<String, Integer> asd = new HashMap<String, Integer>();
            listSubCategory.forEach(subCategory -> {
                int val = bhabinReportRepository.countReportByCategory(subCategory.getName(), polres.getName());
                asd.put(subCategory.getName(), val);
                sumList.add(val);
            });
            recapSubCategoryDtoArrayList.add(new RecapSumOnlyDto(polres.getName(), asd));
        });
        int total = sumList.stream().mapToInt(Integer::intValue).sum();
        recap.setTotal(total);
        recap.setDetails(recapSubCategoryDtoArrayList);
        return recap;


    }

    @Override
    public ReportRecapSumDto getReportRecapSumOnly(PageRequest pageRequest, String category) throws ReportException {
        if (bhabinReportCategoryRepository.findAllByDeletedAtIsNull(pageRequest) == null) {
            throw new ReportException("No Category to be Processed");
        }

        BhabinReportCategory reportCategory = bhabinReportCategoryRepository.findByNameAndDeletedAtIsNull(category);
        List<Polres> polresList = polresRepository.findAllByDeletedAtIsNullOrderByNameAsc();
        List<RecapSubCategoryDto> recapSubCategoryDtoArrayList = new ArrayList<>();
        final List<BhabinReportCategory> listSubCategory = new ArrayList<>();
        List<Integer> sumList = new ArrayList<>();
        List<ReportRecapSumOnlyDto> listSums = new ArrayList<>();
        ReportRecapSumDto recap = new ReportRecapSumDto();
        recap.setCategory(category);


        if (reportCategory.getSubcategories().size() > 0) {
            reportCategory.getSubcategories().forEach(sub -> {
                listSubCategory.add(sub);
            });
        } else if (reportCategory.getSubcategories().size() == 0) {
            listSubCategory.add(reportCategory);
        }

        polresList.forEach(polres -> {
            List<Integer> listSumPerSubCategory = new ArrayList<Integer>();
            listSubCategory.forEach(subCategory -> {
                int val = bhabinReportRepository.countReportByCategory(subCategory.getName(), polres.getName());
                sumList.add(val);
                listSumPerSubCategory.add(val);
            });
            int sumPerSub = listSumPerSubCategory.stream().mapToInt(Integer::intValue).sum();
            listSums.add(new ReportRecapSumOnlyDto(polres.getName(), sumPerSub));
        });
        int total = sumList.stream().mapToInt(Integer::intValue).sum();
        recap.setTotal(total);
        recap.setDetails(listSums);
        return recap;

    }
}
