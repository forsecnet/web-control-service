/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.LaporanDto;
import com.forsecnet.polri.dashboardback.dto.MustReportCountCategoryDto;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.exception.ReportQueryException;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinProfileRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustCommentRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustPositionRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustProfileRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.forsecnet.polri.dashboardback.repository.TierThreeRegionRepository;

@Service
@Transactional(Transactional.TxType.REQUIRED)
public class MustReportServiceImpl extends DtoServiceImpl<LaporanDto, MustReport, Long, MustReportRepository> implements MustReportService {

    private static final Logger LOGGER = Logger.getLogger(MustReportServiceImpl.class);
    
    @Autowired
    private MustReportRepository mustReportRepository;

    @Autowired
    private PolresRepository polresRepository;

    @Autowired
    private MustPositionRepository mustPositionRepository;
    
    @Autowired
    private TierThreeRegionRepository regionRepository;

    @Autowired
    private MustProfileRepository mustProfileRepository;

    @Autowired
    private MustCommentRepository mustCommentRepository;

    @Autowired
    private BhabinProfileRepository bhabinProfileRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;
    
    @PersistenceContext
    private EntityManager entityManager;

    @Value("${ebhabin.must.path.base}")
    private String baseUrl;

    @Value("${ebhabin.must.path.picture.profile}")
    private String pictureUrl;

    @Value("${ebhabin.must.path.picture.report}")
    private String reportPictureUrl;

    @Value("${ebhabin.must.store.picture.report}")
    private String reportPictureStore;

    @Override
    @Autowired
    public void setJpaRepository(MustReportRepository mustReportRepository) {
        super.setJpaRepository(mustReportRepository);
    }

    @Override
    public LaporanDto getAndConvertToDTO(Long id) {
        LaporanDto laporanDTO = super.getAndConvertToDTO(id);

        List<String> pictureFiles = laporanDTO.getPicturePath();
        ArrayList<String> picturePaths = new ArrayList<>();

        pictureFiles.stream().forEach((picture) -> {
            picturePaths.add(baseUrl + reportPictureUrl + "/" + laporanDTO.getId() + "/" + picture);
        });

        laporanDTO.setPicturePath(picturePaths);

        return laporanDTO;
    }

    @Override
    public LaporanDto getAndConvertToDTO(Long id, Long mustProfileId) throws ReportQueryException {
        MustReport mustReport = mustReportRepository.findOneInContext(id, mustProfileId);

        if (mustReport == null) {
            throw new ReportQueryException("Report not found");
        }

        LaporanDto laporanDTO = new LaporanDto(mustReport);

        List<String> pictureFiles = laporanDTO.getPicturePath();
        ArrayList<String> picturePaths = new ArrayList<>();

        pictureFiles.stream().forEach((picture) -> {
            picturePaths.add(baseUrl + reportPictureUrl + "/" + laporanDTO.getId() + "/" + picture);
        });

        laporanDTO.setPicturePath(picturePaths);

        return laporanDTO;
    }

    @Override
    public List<LaporanDto> listAndConvertToDTO(PageRequest pageRequest) {
        List<MustReport> listReports = super.list(pageRequest);
        ArrayList<LaporanDto> listLaporan = new ArrayList<>();

        listReports.stream().map((report) -> new LaporanDto(report)).forEach((laporanDTO) -> {
            List<String> pictureFiles = laporanDTO.getPicturePath();
            ArrayList<String> picturePaths = new ArrayList<>();

            pictureFiles.stream().forEach((picture) -> {
                picturePaths.add(baseUrl + reportPictureUrl + "/" + laporanDTO.getId() + "/" + picture);
            });

            laporanDTO.setPicturePath(picturePaths);

            listLaporan.add(laporanDTO);
        });

        return listLaporan;
    }

    @Override
    public List<LaporanDto> listAndConvertToDTO(PageRequest pageRequest, Long mustProfileId) throws ReportQueryException {
        List<MustReport> listReports = mustReportRepository.findAllInContext(pageRequest, mustProfileId).getContent();

        if (listReports == null) {
            throw new ReportQueryException("Reports not found");
        }

        ArrayList<LaporanDto> listLaporan = new ArrayList<>();

        listReports.stream().map((report) -> new LaporanDto(report)).forEach((laporanDTO) -> {
            List<String> pictureFiles = laporanDTO.getPicturePath();
            ArrayList<String> picturePaths = new ArrayList<>();

            pictureFiles.stream().forEach((picture) -> {
                picturePaths.add(baseUrl + reportPictureUrl + "/" + laporanDTO.getId() + "/" + picture);
            });

            laporanDTO.setPicturePath(picturePaths);

            listLaporan.add(laporanDTO);
        });

        return listLaporan;
    }

    @Override
    public MustReportCountCategoryDto listReportByCategoryAndPolresAndConvertToDto(String polres) throws ReportException {
        Map<String, Long> scoresMap = new HashMap<>();
        if (polresRepository.findByNameAndDeletedAtIsNull(polres) == null) {
            throw new ReportException("no polres found");
        }
        //List<MustReport> mustReportList = mustReportRepository.findAllByCategoryAndDeletedAtIsNull(MustReport.Category.valueOf(category));
        List<String> categoryList = Stream.of(MustReport.Category.values()).map(MustReport.Category::name).collect(Collectors.toList());
        categoryList.forEach(category -> {
            long value = mustReportRepository.countReportCountByPolresAndCategory(polres, category);
            scoresMap.put(category, value);
        });
        return new MustReportCountCategoryDto(polres, scoresMap);

    }


}
