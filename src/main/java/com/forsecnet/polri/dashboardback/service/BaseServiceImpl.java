/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import java.io.Serializable;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author user
 */
public abstract class BaseServiceImpl<E, PK extends Serializable, R extends JpaRepository<E, PK>> implements BaseService<E, PK, R>{
    
    private static final Logger LOGGER = Logger.getLogger(BaseServiceImpl.class);
    
    protected R jpaRepository;

    public void setJpaRepository(R jpaRepository){
        this.jpaRepository = jpaRepository;
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public E create(E data){
       return this.jpaRepository.save(data);
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public E update(E data){
        return this.jpaRepository.save(data);
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public E get(PK id) {
        return this.jpaRepository.findOne(id);
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public List<E> list(PageRequest pageRequest) {
        return this.jpaRepository.findAll(pageRequest).getContent();
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public E delete(E entity) {
        this.jpaRepository.delete(entity);
        return entity;
    }

}
