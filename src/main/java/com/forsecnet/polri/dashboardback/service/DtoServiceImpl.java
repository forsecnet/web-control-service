/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.BaseDto;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;


public class DtoServiceImpl<D extends BaseDto<E>, E, PK extends Serializable, R extends JpaRepository<E, PK>> extends BaseServiceImpl<E, PK, R> implements DtoService<D, E, PK, R> {
    
    private static final Logger LOGGER = Logger.getLogger(DtoServiceImpl.class);
    
    private final Class<D> dtoClazz;
    private final Class<E> entityClazz;
    
    public DtoServiceImpl() {
        this.dtoClazz = (Class<D>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.entityClazz = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public D getAndConvertToDTO(PK id) {
        try {
            LOGGER.debug("Entity class: " + entityClazz.getName());
            LOGGER.debug("DTO class: " + dtoClazz.getName());
            E e = this.jpaRepository.findOne(id);
            Constructor<D> dconst = dtoClazz.getConstructor(this.entityClazz);
            D c = dconst.newInstance(e);
            return c;
        } catch (NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | InvocationTargetException ex) {
            LOGGER.error("DTO mapping error...", ex);
            throw new RuntimeException("DTO mapping error...", ex);
        }
    }
    
    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public List<D> listAndConvertToDTO(PageRequest pageRequest) {
        try {
            LOGGER.debug("Entity class: " + entityClazz.getName());
            LOGGER.debug("DTO class: " + dtoClazz.getName());
            List<E> es = this.jpaRepository.findAll(pageRequest).getContent();
            Constructor<D> dconst = dtoClazz.getConstructor(this.entityClazz);
            
            ArrayList<D> ds = new ArrayList<>();
            for (E e : es) {
                ds.add(dconst.newInstance(e));
            }
            return ds;
        } catch (NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | InvocationTargetException ex) {
            LOGGER.error("DTO mapping error...", ex);
            throw new RuntimeException("DTO mapping error...", ex);
        }
    }

}
