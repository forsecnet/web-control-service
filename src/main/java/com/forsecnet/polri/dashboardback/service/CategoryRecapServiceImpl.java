package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.CategoryRecapDto;
import com.forsecnet.polri.dashboardback.dto.ReportRecapDto;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by BQA on 10/21/16.
 */
@Service
@Transactional(Transactional.TxType.REQUIRED)
public class CategoryRecapServiceImpl extends DtoServiceImpl<CategoryRecapDto, MustReport, Long, MustReportRepository> implements CategoryRecapService {

    @Autowired
    MustReportRepository mustReportRepository;

    @Override
    @Autowired
    public void setJpaRepository(MustReportRepository mustReportRepository) {
        super.setJpaRepository(mustReportRepository);
    }

    @Override
    public CategoryRecapDto getRecapAndConvertToDto(PageRequest pageRequest) {
        if(mustReportRepository.findAllByDeletedAtIsNull(pageRequest) == null){
            throw new ReportException("No Report to Be Processed");
        }

        MustReport simpleReport = mustReportRepository.findAllByDeletedAtIsNull(pageRequest).getContent().get(0);
        CategoryRecapDto categoryRecapDto = new CategoryRecapDto(simpleReport);
        Map<MustReport.Category,Integer> recapMap = new HashMap<>();
        categoryRecapDto.getCategory().forEach(category -> {
            recapMap.put(category,mustReportRepository.countByCategory(category));
        });

        categoryRecapDto.setData(recapMap);
        return categoryRecapDto;
    }

}
