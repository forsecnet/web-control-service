/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.service;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author user
 */
public interface BaseService<E, PK extends Serializable, R extends JpaRepository<E, PK>> {
    
    public E create(E data);
    
    public E update(E data);
    
    public E get(PK id);
    
    public List<E> list(PageRequest pageRequest);
    
    public E delete(E entity);
    
}
