package com.forsecnet.polri.dashboardback.service;

import com.forsecnet.polri.dashboardback.dto.CategoryRecapDto;
import com.forsecnet.polri.dashboardback.dto.ReportRecapDto;
import com.forsecnet.polri.dashboardback.exception.ReportException;
import com.forsecnet.polri.dashboardback.model.must.MustReport;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import org.springframework.data.domain.PageRequest;

/**
 * Created by BQA on 10/21/16.
 */
public interface CategoryRecapService extends DtoService<CategoryRecapDto, MustReport, Long, MustReportRepository> {

    public CategoryRecapDto getRecapAndConvertToDto(PageRequest pageRequest) throws ReportException;


}
