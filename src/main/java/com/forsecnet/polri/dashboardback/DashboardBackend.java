package com.forsecnet.polri.dashboardback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaRepositories("com.forsecnet.polri.dashboardback.repository")
@EnableScheduling
public class DashboardBackend {

	public static void main(String[] args) {
		SpringApplication.run(DashboardBackend.class, args);
	}
}
