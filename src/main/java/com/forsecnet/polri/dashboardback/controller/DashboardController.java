package com.forsecnet.polri.dashboardback.controller;

import com.forsecnet.polri.dashboardback.dto.*;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import com.forsecnet.polri.dashboardback.repository.PolsekRepository;
import com.forsecnet.polri.dashboardback.repository.bhabin.BhabinProfileRepository;
import com.forsecnet.polri.dashboardback.repository.must.MustReportRepository;
import com.forsecnet.polri.dashboardback.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/api/dashboard/")
public class DashboardController {

    @Autowired
    MustReportRepository mustReportRepository;

    @Autowired
    BhabinProfileRepository bhabinProfileRepository;

    @Autowired
    MustReportService mustReportService;

    @Autowired
    BhabinProfileService bhabinProfileService;

    @Autowired
    CategoryRecapService categoryRecapService;

    @Autowired
    ReportRecapService reportRecapService;

    @Autowired
    PolresRepository polresRepository;

    @Autowired
    PolsekRepository polsekRepository;

    @Autowired
    PolresService polresService;

    @Autowired
    MustProfileService mustProfileService;

    @Autowired
    MustCredentialService mustCredentialService;

    @RequestMapping(method = RequestMethod.GET, value = "/report/masyarakat/list")
    public ResponseEnvelopeDto<List<LaporanDto>> getAllMustReport(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp) {
        List<LaporanDto> listLaporan = mustReportService.listAndConvertToDTO(new PageRequest(page, size, Sort.Direction.DESC, sortProp));

        return new ResponseEnvelopeDto(true, "Return type: List<LaporanDTO>", listLaporan);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/list")
    public ResponseEnvelopeDto<List<BhabinProfileDto>> getAllBhabinProfile(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp) {
        List<BhabinProfileDto> listProfile = bhabinProfileService.listAndConvertToDTO(new PageRequest(page, size, Sort.Direction.DESC, sortProp));

        return new ResponseEnvelopeDto(true, "Return type: List<BhabinProfileDto>", listProfile);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/masyarakat/recap")
    public ResponseEnvelopeDto<CategoryRecapDto> getCategoryRecap(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp) {
        CategoryRecapDto categoryRecapDto = categoryRecapService.getRecapAndConvertToDto(new PageRequest(page, size, Sort.Direction.DESC, sortProp));

        return new ResponseEnvelopeDto<>(true, "Return Type: CategoryRecapDto", categoryRecapDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/{id}")
    public ResponseEnvelopeDto<BhabinProfileDto> getBhabinById(@PathVariable long id) {
        return new ResponseEnvelopeDto<>(true, "Return Type: BhabinProfileDto", bhabinProfileService.getAndConvertToDTO(id));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/recap2/{category}")
    public ResponseEnvelopeDto<ReportRecap2Dto> getRecap2ByCategory(
            @PathVariable String category,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp
    ) {
        ReportRecap2Dto reportRecapDto = reportRecapService.getReportRecapByPolresAndConvertToDto(new PageRequest(page, size, Sort.Direction.DESC, sortProp), category);

        return new ResponseEnvelopeDto<>(true, "Return Type: ReportRecapDto", reportRecapDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/recap/{category}")
    public ResponseEnvelopeDto<ReportRecapDto> getRecapByCategory(
            @PathVariable String category,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp
    ) {
        ReportRecapDto reportRecapDto = reportRecapService.getReportRecapAndConvertToDto(new PageRequest(page, size, Sort.Direction.DESC, sortProp), category);

        return new ResponseEnvelopeDto<>(true, "Return Type: ReportRecapDto", reportRecapDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/recap-sum/{category}")
    public ResponseEnvelopeDto<ReportRecapSumDto> getRecapSumOnlyByCategory(
            @PathVariable String category,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp
    ) {
        ReportRecapSumDto reportRecapDto = reportRecapService.getReportRecapSumOnly(new PageRequest(page, size, Sort.Direction.DESC, sortProp), category);

        return new ResponseEnvelopeDto<>(true, "Return Type: ReportRecapSumOnlyDto", reportRecapDto);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/bhabin/register")
    public ResponseEnvelopeDto<BhabinProfileDto> registerUser(
            @RequestParam String name,
            @RequestParam String nrp,
            @RequestParam String rank,
            @RequestParam String telepon,
            @RequestParam String password,
            @RequestParam String alamat,
            @RequestParam String polsek,
            @RequestParam String polres,
            @RequestParam String location
    ) {
        BhabinProfileDto bhabinProfileDto = bhabinProfileService.registerUser(name, nrp, rank, telepon, password, alamat, polsek, polres, location);
        return new ResponseEnvelopeDto<BhabinProfileDto>(true, "ReturnType: BhabinProfileDto", bhabinProfileDto);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/bhabin/update/{id}")
    public ResponseEnvelopeDto<BhabinProfileDto> updateUser(
            @PathVariable long id,
            @RequestParam String name,
            @RequestParam String rank,
            @RequestParam String telepon,
            @RequestParam String password,
            @RequestParam String alamat,
            @RequestParam String polsek,
            @RequestParam String polres,
            @RequestParam String location,
            @RequestParam String picturePath
    ) {
        BhabinProfileDto bhabinProfileDto = bhabinProfileService.updateUser(id, name, rank, telepon, password, alamat, polsek, polres, picturePath, location);
        return new ResponseEnvelopeDto<BhabinProfileDto>(true, "ReturnType: BhabinProfileDto", bhabinProfileDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/polres/list")
    public ResponseEnvelopeDto<List<PolresDto>> listPolresWithPolsek() {
        List<PolresDto> polresDto = bhabinProfileService.listPolresWithCorrespondingPolsekAndConvertToDto(new PageRequest(0, Integer.MAX_VALUE, Sort.Direction.DESC, "id"));
        return new ResponseEnvelopeDto<List<PolresDto>>(true, "Return Type: PolresDto", polresDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/polres/average-response")
    public ResponseEnvelopeDto<List<AverageResponseDto>> getAverageResponseList(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp) {
        List<AverageResponseDto> listAveragePolres = polresService.getAverageByResponseTimeList(new PageRequest(page, size, Sort.Direction.DESC, sortProp));

        return new ResponseEnvelopeDto(true, "Return type: List<AverageResponseDto>", listAveragePolres);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/report/rejected")
    public ResponseEnvelopeDto<List<RejectedReportDto>> getRejectedReportsWithProfile(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "id") String sortProp) {
        List<RejectedReportDto> rejectedReportDtos = mustCredentialService.getRejectedReportsOwnerProfileList(new PageRequest(page, size, Sort.Direction.DESC, sortProp));
        return new ResponseEnvelopeDto<>(true,"Return type: List<RejectedReportDto>",rejectedReportDtos);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/ban/{id}")
    public ResponseEnvelopeDto<MustCredentialDto> banMasyarakat(@PathVariable long id){
        MustCredentialDto bannedMasyarakat = mustCredentialService.banMasyarakatsAndConverToDto(id);
        return new ResponseEnvelopeDto<>(true, "Return type: MustCredentialDto",bannedMasyarakat);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/masyarakat/ranking")
    public ResponseEnvelopeDto<List<RankingDto>> getMasyarakatRanking() {
        return new ResponseEnvelopeDto<>(true, "Return Type: List<RankingDto>", mustProfileService.getRanking(10, 0));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/masyarakat/{id}")
    public ResponseEnvelopeDto<MustProfileDto> getMasyarakat(@PathVariable long id){
        MustProfileDto mustProfileDto = mustProfileService.getMasyarakatWithPointsAndConvertToDto(id);
        return new ResponseEnvelopeDto<>(true,"Return Type: MustProfileDto", mustProfileDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/bhabin/delete/{id}")
    public ResponseEnvelopeDto<BhabinProfileDto> deleteBhabin(@PathVariable long id) {
        BhabinProfileDto bhabinProfileDto = bhabinProfileService.deleteBhabinAndConvertToDto(id);
        return new ResponseEnvelopeDto<>(true, "Return Type: BhabinProfileDto", bhabinProfileDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bhabin/ranking")
    public ResponseEnvelopeDto<List<BhabinRankDto>> getBhabinRanking() {
        return new ResponseEnvelopeDto<>(true, "Return Type: List<BhabinRankDto>", bhabinProfileService.getRanking(10, 0));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/report/masyarakat/{polres}")
    public ResponseEnvelopeDto<MustReportCountCategoryDto> getReportCountPerCategory(@PathVariable String polres) {
        return new ResponseEnvelopeDto<>(true, "Return Type: List<MustReportCountCategoryDto>", mustReportService.listReportByCategoryAndPolresAndConvertToDto(polres));
    }


}
