/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.exception;

/**
 *
 * @author user
 */
public class ReportQueryException extends ReportException {

    public ReportQueryException() {
    }

    public ReportQueryException(String message) {
        super(message);
    }

    public ReportQueryException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReportQueryException(Throwable cause) {
        super(cause);
    }
    
}
