/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.exception;

/**
 *
 * @author user
 */
public class ProfileQueryException extends ProfileException {

    public ProfileQueryException() {
    }

    public ProfileQueryException(String message) {
        super(message);
    }

    public ProfileQueryException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProfileQueryException(Throwable cause) {
        super(cause);
    }
    
}
