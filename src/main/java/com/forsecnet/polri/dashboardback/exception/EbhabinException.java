/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.exception;

/**
 *
 * @author user
 */
public class EbhabinException extends RuntimeException{

    public EbhabinException() {
    }

    public EbhabinException(String message) {
        super(message);
    }

    public EbhabinException(String message, Throwable cause) {
        super(message, cause);
    }

    public EbhabinException(Throwable cause) {
        super(cause);
    }

}
