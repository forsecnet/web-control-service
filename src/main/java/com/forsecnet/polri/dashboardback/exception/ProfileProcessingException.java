/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.exception;

/**
 *
 * @author user
 */
public class ProfileProcessingException extends ProfileException {

    public ProfileProcessingException() {
    }

    public ProfileProcessingException(String message) {
        super(message);
    }

    public ProfileProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProfileProcessingException(Throwable cause) {
        super(cause);
    }
    
}
