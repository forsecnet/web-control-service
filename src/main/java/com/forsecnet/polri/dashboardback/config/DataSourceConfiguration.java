/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.config;

import com.forsecnet.polri.dashboardback.repository.RefreshableRepositoryImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author user
 */
@Configuration
@EnableJpaRepositories(repositoryBaseClass = RefreshableRepositoryImpl.class)
@EnableTransactionManagement
public class DataSourceConfiguration {

    private static final Logger LOGGER = Logger.getLogger(DataSourceConfiguration.class);

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;
    
    @Value("${spring.datasource.pool.min}")
    private int poolMin;
    
    @Value("${spring.datasource.pool.max}")
    private int poolMax;

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setMaximumPoolSize(poolMax);
        hikariConfig.setMinimumIdle(poolMin);
        hikariConfig.setPoolName("ebhabin-pool");
        hikariConfig.setAutoCommit(false);

        return new HikariDataSource(hikariConfig);
    }
}