package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

import java.util.List;

/**
 * Created by BQA on 10/21/16.
 */
public class ReportRecap2Dto extends BaseDto<BhabinReport> {
    private String category;
    private int total;
    private List<RecapSumOnlyDto> details;

    public ReportRecap2Dto() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<RecapSumOnlyDto> getDetails() {
        return details;
    }

    public void setDetails(List<RecapSumOnlyDto> data) {
        this.details = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
