/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustProfile;

/**
 *
 * @author user
 */
public class RankingDto extends BaseDto<MustProfile> implements Comparable<RankingDto>{
    
    private long rank;
    private long id;
    private String name;
    private String lokasi;
    private long merah;
    private long kuning;
    private long hijau;
    private long total;
    private long refreshedAt;

    public RankingDto() {
    }
    
    public RankingDto(MustProfile mustProfile, PointDto pointDTO) {
        this.id = mustProfile.getId();
        this.name = mustProfile.getName();
        this.lokasi = mustProfile.getRegion().getName();
        this.merah = pointDTO.getMerah();
        this.kuning = pointDTO.getKuning();
        this.hijau = pointDTO.getHijau();
        this.total = pointDTO.getTotal();
        this.refreshedAt = pointDTO.getRefreshedAt();
    }

    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public long getMerah() {
        return merah;
    }

    public void setMerah(long merah) {
        this.merah = merah;
    }

    public long getKuning() {
        return kuning;
    }

    public void setKuning(long kuning) {
        this.kuning = kuning;
    }

    public long getHijau() {
        return hijau;
    }

    public void setHijau(long hijau) {
        this.hijau = hijau;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getRefreshedAt() {
        return refreshedAt;
    }

    public void setRefreshedAt(long refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    @Override
    public int compareTo(RankingDto o) {
        return Math.toIntExact(o.getTotal() - this.total);
    }
    
}
