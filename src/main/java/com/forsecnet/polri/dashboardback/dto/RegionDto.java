/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.TierThreeRegion;

/**
 *
 * @author user
 */
public class RegionDto extends BaseDto<TierThreeRegion>{
    
    private long id;
    private String name;
    private String kecamatan;

    public RegionDto(TierThreeRegion region) {
        this.id = region.getId();
        this.name = region.getName();
        this.kecamatan = region.getKecamatan().getName();
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    
}
