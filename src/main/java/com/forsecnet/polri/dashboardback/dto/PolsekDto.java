package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.Polsek;

/**
 * Created by BQA on 10/22/16.
 */
public class PolsekDto extends BaseDto<Polsek> {
    private String polsek;

    public PolsekDto() {
    }

    public PolsekDto(Polsek polsek) {
        this.polsek = polsek.getName();
    }

    public String getPolsek() {
        return polsek;
    }

    public void setPolsek(String polsek) {
        this.polsek = polsek;
    }
}
