package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.Polres;
import com.forsecnet.polri.dashboardback.model.Polsek;
import com.forsecnet.polri.dashboardback.repository.PolresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.*;

/**
 * Created by BQA on 10/22/16.
 */
public class PolresDto extends BaseDto<Polres> {


    private String polres;
    private List<String> polsekList;

    public PolresDto() {
    }

    public PolresDto(Polres polres,List<Polsek> polseks) {
        this.polres = polres.getName();
        this.polsekList = new ArrayList<>();
        polseks.stream().map((polsek) -> {
            String polsekName = polsek.getName();
            return polsekName;
        }).forEach((polsekName) -> {
            this.polsekList.add(polsekName);
        });
    }

    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public List<String> getPolsek() {
        return polsekList;
    }

    public void setPolsek(List<String> polsek) {
        this.polsekList = polsek;
    }
}
