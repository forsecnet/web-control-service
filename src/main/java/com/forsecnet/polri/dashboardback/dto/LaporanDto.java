/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustReport;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author user
 */
public class LaporanDto extends BaseDto<MustReport> {

    private long id;
    private String namaPelapor;
    private String title;
    private String description;
    private List<String> picturePath;
    private String category;
    private long date;
    private String status;
    private double latitude;
    private double longitude;

    public LaporanDto() {
    }
    
    public LaporanDto(MustReport mustReport) {
        this.id = mustReport.getId();
        this.namaPelapor = mustReport.getMustPosition().getMustProfile().getName();
        this.title = mustReport.getTitle();
        this.description = mustReport.getDescription();
        this.picturePath = mustReport.getReportedPictureFiles().stream().map(f -> f.split("\\.")[0]).collect(Collectors.toList());
        this.category = mustReport.getCategory().toString();
        this.date = mustReport.getCreatedAt().getTime();
        this.status = mustReport.getStatus().toString();
        this.latitude = mustReport.getMustPosition().getLatitude();
        this.longitude = mustReport.getMustPosition().getLongitude();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamaPelapor() {
        return namaPelapor;
    }

    public void setNamaPelapor(String namaPelapor) {
        this.namaPelapor = namaPelapor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(List<String> picturePath) {
        this.picturePath = picturePath;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
