package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

import java.util.List;

/**
 * Created by BQA on 10/27/16.
 */
public class ReportRecapSumDto extends BaseDto<BhabinReport> {
    private String category;
    private int total;
    private List<ReportRecapSumOnlyDto> details;

    public ReportRecapSumDto() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<ReportRecapSumOnlyDto> getDetails() {
        return details;
    }

    public void setDetails(List<ReportRecapSumOnlyDto> data) {
        this.details = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
