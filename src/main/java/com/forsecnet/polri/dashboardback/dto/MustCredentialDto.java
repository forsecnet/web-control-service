package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustCredential;

/**
 * Created by BQA on 10/25/16.
 */
public class MustCredentialDto extends BaseDto<MustCredential> {
    private String nama;
    private String picturePath;
    private String noKtp;
    private String noHp;
    private String lokasi;

    public MustCredentialDto() {
    }

    public MustCredentialDto(MustCredential mustCredential) {
        this.nama = mustCredential.getMustProfile().getName();
        this.noKtp = mustCredential.getMustProfile().getCitizenId();
        this.noHp = mustCredential.getMobileNumber();
        this.picturePath = mustCredential.getMustProfile().getPictureFile().split("\\.")[0];
        this.lokasi = mustCredential.getMustProfile().getRegion().getName();
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
}
