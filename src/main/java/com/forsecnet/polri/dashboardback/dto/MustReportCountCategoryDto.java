package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustReport;

import java.util.Map;

/**
 * Created by BQA on 11/2/16.
 */
public class MustReportCountCategoryDto extends BaseDto<MustReport> {
    private String polres;
    private Map<String, Long> scores;

    public MustReportCountCategoryDto() {
    }

    public MustReportCountCategoryDto(String polres, Map<String, Long> scores) {
        this.polres = polres;
        this.scores = scores;
    }

    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public Map<String, Long> getScores() {
        return scores;
    }

    public void setScores(Map<String, Long> scores) {
        this.scores = scores;
    }
}
