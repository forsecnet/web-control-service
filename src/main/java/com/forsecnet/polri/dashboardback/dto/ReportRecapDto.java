package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

import java.util.List;
import java.util.Map;

/**
 * Created by BQA on 10/21/16.
 */
public class ReportRecapDto extends BaseDto<BhabinReport> {
    private String category;
    private int total;
    private List<RecapSubCategoryDto> details;

    public ReportRecapDto() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<RecapSubCategoryDto> getDetails() {
        return details;
    }

    public void setDetails(List<RecapSubCategoryDto> data) {
        this.details = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
