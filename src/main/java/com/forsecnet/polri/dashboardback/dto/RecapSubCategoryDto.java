package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

import java.util.Map;

/**
 * Created by BQA on 10/26/16.
 */
public class RecapSubCategoryDto extends BaseDto<BhabinReport> {
    private String category;
    private Map<String, Integer> listPolres;

    public RecapSubCategoryDto(String category, Map<String, Integer> listPolres) {
        this.category = category;
        this.listPolres = listPolres;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Map<String, Integer> getListPolres() {
        return listPolres;
    }

    public void setListPolres(Map<String, Integer> listPolres) {
        this.listPolres = listPolres;
    }
}
