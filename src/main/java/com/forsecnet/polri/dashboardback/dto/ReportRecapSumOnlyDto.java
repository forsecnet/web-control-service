package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

/**
 * Created by BQA on 10/27/16.
 */
public class ReportRecapSumOnlyDto extends BaseDto<BhabinReport> {
    private String polres;
    private int sum;

    public ReportRecapSumOnlyDto() {
    }

    public ReportRecapSumOnlyDto(String polres, int sum) {
        this.polres = polres;
        this.sum = sum;
    }

    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
