package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinReport;

import java.util.Map;

/**
 * Created by BQA on 10/27/16.
 */
public class RecapSumOnlyDto extends BaseDto<BhabinReport> {
    private String polres;
    private Map<String, Integer> listCategory;

    public RecapSumOnlyDto(String polres, Map<String, Integer> listCategory) {
        this.polres = polres;
        this.listCategory = listCategory;
    }

    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public Map<String, Integer> getListCategory() {
        return listCategory;
    }

    public void setListCategory(Map<String, Integer> listCategory) {
        this.listCategory = listCategory;
    }
}
