/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustProfile;

/**
 *
 * @author user
 */
public class MustProfileDto extends BaseDto<MustProfile>{
    
    private String nama;
    private String picturePath;
    private String noKtp;
    private String noHp;
    private String lokasi;
    private PointDto point;

    public MustProfileDto() {
    }
    
    public MustProfileDto(MustProfile mustProfile) {
        this.nama = mustProfile.getName();
        this.noKtp = mustProfile.getCitizenId();
        this.noHp = mustProfile.getMustCredential().getMobileNumber();
        this.picturePath = mustProfile.getPictureFile().split("\\.")[0];
        this.lokasi = mustProfile.getRegion().getName();
    }
    
    public MustProfileDto(MustProfile mustProfile, PointDto point) {
        this.nama = mustProfile.getName();
        this.noKtp = mustProfile.getCitizenId();
        this.noHp = mustProfile.getMustCredential().getMobileNumber();
        this.picturePath = mustProfile.getPictureFile().split("\\.")[0];
        this.lokasi = mustProfile.getRegion().getName();
        this.point = point;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public PointDto getPoint() {
        return point;
    }

    public void setPoint(PointDto point) {
        this.point = point;
    }

}
