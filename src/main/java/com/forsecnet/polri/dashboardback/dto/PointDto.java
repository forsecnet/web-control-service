/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

/**
 *
 * @author user
 */
public class PointDto extends BaseDto<Object>{
    
    private long rank;
    private long merah;
    private long kuning;
    private long hijau;
    private long total;
    private long refreshedAt;

    public PointDto() {
    }
    
    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {    
        this.rank = rank;
    }

    public long getMerah() {
        return merah;
    }

    public void setMerah(long merah) {
        this.merah = merah;
    }

    public long getKuning() {
        return kuning;
    }

    public void setKuning(long kuning) {
        this.kuning = kuning;
    }

    public long getHijau() {
        return hijau;
    }

    public void setHijau(long hijau) {
        this.hijau = hijau;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getRefreshedAt() {
        return refreshedAt;
    }

    public void setRefreshedAt(long refreshedAt) {
        this.refreshedAt = refreshedAt;
    }
    
}
