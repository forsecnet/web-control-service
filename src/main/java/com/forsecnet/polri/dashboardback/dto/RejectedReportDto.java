package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustProfile;
import com.forsecnet.polri.dashboardback.model.must.MustReport;

import java.sql.Timestamp;

/**
 * Created by BQA on 10/25/16.
 */
public class RejectedReportDto extends BaseDto<MustReport> {
    private long profileId;
    private String profileName;
    private String reportTitle;
    private String reportDescription;
    private Timestamp reportCreatedAt;

    public RejectedReportDto() {
    }

    public RejectedReportDto(MustReport mustReport) {
        this.profileId = mustReport.getMustPosition().getMustProfile().getId();
        this.profileName = mustReport.getMustPosition().getMustProfile().getName();
        this.reportCreatedAt = mustReport.getCreatedAt();
        this.reportTitle = mustReport.getTitle();
        this.reportDescription = mustReport.getDescription();
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getReportDescription() {
        return reportDescription;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public Timestamp getReportCreatedAt() {
        return reportCreatedAt;
    }

    public void setReportCreatedAt(Timestamp reportCreatedAt) {
        this.reportCreatedAt = reportCreatedAt;
    }
}
