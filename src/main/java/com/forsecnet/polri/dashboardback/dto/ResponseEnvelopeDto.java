/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.forsecnet.polri.dashboardback.dto;

/**
 *
 * @author user
 */
public class ResponseEnvelopeDto<E> {
    
    private boolean status;
    
    private String message;
    
    private E data;

    public ResponseEnvelopeDto() {
    }

    public ResponseEnvelopeDto(boolean status, String message, E data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
    
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }
    
}
