package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.must.MustReport;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Created by BQA on 10/21/16.
 */
public class CategoryRecapDto extends BaseDto<MustReport>{

    @Autowired
    MustReport mustReport;

    private List<MustReport.Category> category;
    private Map<MustReport.Category,Integer> data;

    public CategoryRecapDto() {

    }

    public CategoryRecapDto(MustReport mustReport) {
        this.category = Arrays.asList(mustReport.getCategory().values());
    }

    public List<MustReport.Category> getCategory() {
        return category;
    }

    public void setCategory(List<MustReport.Category> category) {
        this.category = category;
    }

    public Map<MustReport.Category, Integer> getData() {
        return data;
    }

    public void setData(Map<MustReport.Category, Integer> data) {
        this.data = data;
    }
}
