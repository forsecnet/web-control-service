package com.forsecnet.polri.dashboardback.dto;

/**
 * Created by BQA on 11/2/16.
 */
public class BhabinPointDto {

    private long rank;
    private long mustPoint;
    private long bhabinPoint;
    private long dds;
    private long giatKreatif;
    private long problemSolving;
    private long total;
    private long refreshedAt;

    public BhabinPointDto() {
    }

    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    public long getDds() {
        return dds;
    }

    public void setDds(long dds) {
        this.dds = dds;
    }

    public long getGiatKreatif() {
        return giatKreatif;
    }

    public void setGiatKreatif(long giatKreatif) {
        this.giatKreatif = giatKreatif;
    }

    public long getProblemSolving() {
        return problemSolving;
    }

    public void setProblemSolving(long problemSolving) {
        this.problemSolving = problemSolving;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getRefreshedAt() {
        return refreshedAt;
    }

    public void setRefreshedAt(long refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    public long getMustPoint() {
        return mustPoint;
    }

    public void setMustPoint(long mustPoint) {
        this.mustPoint = mustPoint;
    }

    public long getBhabinPoint() {
        return bhabinPoint;
    }

    public void setBhabinPoint(long bhabinPoint) {
        this.bhabinPoint = bhabinPoint;
    }
}
