package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.Polres;

/**
 * Created by BQA on 10/25/16.
 */
public class AverageResponseDto extends BaseDto<Polres> {
    private String polres;
    private double average;

    public AverageResponseDto() {
    }

    public AverageResponseDto(Polres polres, double average) {
        this.polres = polres.getName();
        this.average = average;
    }

    public String getPolres() {
        return polres;
    }

    public void setPolres(String polres) {
        this.polres = polres;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
