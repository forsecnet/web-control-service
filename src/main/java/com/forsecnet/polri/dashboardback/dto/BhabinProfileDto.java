package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;

/**
 * Created by BQA on 10/20/16.
 */
public class BhabinProfileDto extends BaseDto<BhabinProfile> {
    private long id;
    private String nama;
    private String pangkat;
    private String nrp;
    private String kesatuan;
    private String polsek;
    private String sebagai;
    private String noHp;
    private String lokasiTugas;
    private String alamatRumah;
    private String picturePath;



    public BhabinProfileDto() {
    }

    public BhabinProfileDto(BhabinProfile bhabinProfile){
        this.id = bhabinProfile.getId();
        this.lokasiTugas = bhabinProfile.getTierThreeRegions().get(0).getName();
        this.kesatuan = bhabinProfile.getPolsek().getPolres().getName();
        this.nama = bhabinProfile.getName();
        this.noHp = bhabinProfile.getPhone();
        this.pangkat = bhabinProfile.getRank().name();
        this.sebagai = bhabinProfile.getCommenterRole();
        this.nrp = bhabinProfile.getBhabinCredential().getNrp();
        this.alamatRumah = bhabinProfile.getHomeAddress();
        this.picturePath = bhabinProfile.getPictureFile();
        this.polsek = bhabinProfile.getPolsek().getName();

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getKesatuan() {
        return kesatuan;
    }

    public void setKesatuan(String kesatuan) {
        this.kesatuan = kesatuan;
    }

    public String getSebagai() {
        return sebagai;
    }

    public void setSebagai(String sebagai) {
        this.sebagai = sebagai;
    }

    public String getLokasiTugas() {
        return lokasiTugas;
    }

    public void setLokasiTugas(String lokasiTugas) {
        this.lokasiTugas = lokasiTugas;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public long getId() {
        return id;
    }

    public void setId(long bhabinId) {
        this.id = bhabinId;
    }

    public String getAlamatRumah() {
        return alamatRumah;
    }

    public void setAlamatRumah(String alamatRumah) {
        this.alamatRumah = alamatRumah;
    }

    public String getPolsek() {
        return polsek;
    }

    public void setPolsek(String polsek) {
        this.polsek = polsek;
    }
}
