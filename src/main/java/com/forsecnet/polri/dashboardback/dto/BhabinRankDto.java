package com.forsecnet.polri.dashboardback.dto;

import com.forsecnet.polri.dashboardback.model.bhabin.BhabinProfile;

/**
 * Created by BQA on 11/2/16.
 */
public class BhabinRankDto extends BaseDto<BhabinProfile> implements Comparable<BhabinRankDto> {

    private long ranking;
    private long id;
    private String jabatan;
    private String name;
    private long mustPoint;
    private long bhabinPoint;
    private long ddsPoint;
    private long problemSolvingPoint;
    private long giatKreatifPoint;
    private long total;
    private long refreshedAt;

    public BhabinRankDto() {
    }

    public BhabinRankDto(BhabinProfile bhabinProfile, BhabinPointDto bhabinPointDto) {
        this.ranking = bhabinPointDto.getRank();
        this.jabatan = bhabinProfile.getRank().toString();
        this.name = bhabinProfile.getName();
        this.bhabinPoint = bhabinPointDto.getBhabinPoint();
        this.mustPoint = bhabinPointDto.getMustPoint();
        this.ddsPoint = bhabinPointDto.getDds();
        this.problemSolvingPoint = bhabinPointDto.getProblemSolving();
        this.giatKreatifPoint = bhabinPointDto.getGiatKreatif();
        this.total = bhabinPointDto.getTotal();
        this.refreshedAt = bhabinPointDto.getRefreshedAt();

    }

    public long getRanking() {
        return ranking;
    }

    public void setRanking(long ranking) {
        this.ranking = ranking;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMustPoint() {
        return mustPoint;
    }

    public void setMustPoint(long mustPoint) {
        this.mustPoint = mustPoint;
    }

    public long getBhabinPoint() {
        return bhabinPoint;
    }

    public void setBhabinPoint(long bhabinPoint) {
        this.bhabinPoint = bhabinPoint;
    }

    public long getDdsPoint() {
        return ddsPoint;
    }

    public void setDdsPoint(long ddsPoint) {
        this.ddsPoint = ddsPoint;
    }

    public long getProblemSolvingPoint() {
        return problemSolvingPoint;
    }

    public void setProblemSolvingPoint(long problemSolvingPoint) {
        this.problemSolvingPoint = problemSolvingPoint;
    }

    public long getGiatKreatifPoint() {
        return giatKreatifPoint;
    }

    public void setGiatKreatifPoint(long giatKreatifPoint) {
        this.giatKreatifPoint = giatKreatifPoint;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getRefreshedAt() {
        return refreshedAt;
    }

    public void setRefreshedAt(long refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int compareTo(BhabinRankDto o) {
        return Math.toIntExact(o.getTotal() - this.total);
    }
}
